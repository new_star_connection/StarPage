# Details

Date : 2020-06-28 20:40:54

Directory d:\tomcat\webapps\starpage

Total : 43 files,  103938 codes, 8574 comments, 11867 blanks, all 124379 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [DinDan.html](/DinDan.html) | HTML | 1,002 | 71 | 40 | 1,113 |
| [GoWuChe.html](/GoWuChe.html) | HTML | 831 | 29 | 39 | 899 |
| [README.en.md](/README.en.md) | Markdown | 25 | 0 | 12 | 37 |
| [README.md](/README.md) | Markdown | 25 | 0 | 13 | 38 |
| [addTo_gowuche.html](/addTo_gowuche.html) | HTML | 4,048 | 69 | 181 | 4,298 |
| [css/DinDan.css](/css/DinDan.css) | CSS | 761 | 11 | 194 | 966 |
| [css/GoWuChe.css](/css/GoWuChe.css) | CSS | 642 | 2 | 17 | 661 |
| [css/addTo_gowuche.css](/css/addTo_gowuche.css) | CSS | 141 | 0 | 6 | 147 |
| [css/iconfont.css](/css/iconfont.css) | CSS | 36 | 0 | 26 | 62 |
| [css/index.css](/css/index.css) | CSS | 2,994 | 66 | 517 | 3,577 |
| [css/list.css](/css/list.css) | CSS | 894 | 4 | 147 | 1,045 |
| [css/login.css](/css/login.css) | CSS | 530 | 7 | 119 | 656 |
| [css/message.css](/css/message.css) | CSS | 2,061 | 7 | 55 | 2,123 |
| [css/my_shop.css](/css/my_shop.css) | CSS | 1,150 | 8 | 194 | 1,352 |
| [css/register.css](/css/register.css) | CSS | 113 | 2 | 1 | 116 |
| [css/theme-chalk/index.css](/css/theme-chalk/index.css) | CSS | 13,043 | 0 | 2,811 | 15,854 |
| [font/iconfont.svg](/font/iconfont.svg) | XML | 153 | 0 | 1 | 154 |
| [index.html](/index.html) | HTML | 2,710 | 1,149 | 82 | 3,941 |
| [js/DinDan.js](/js/DinDan.js) | JavaScript | 142 | 3 | 16 | 161 |
| [js/GoWuChe.js](/js/GoWuChe.js) | JavaScript | 275 | 52 | 34 | 361 |
| [js/axios.min.js](/js/axios.min.js) | JavaScript | 301 | 259 | 28 | 588 |
| [js/cityAndPro.js](/js/cityAndPro.js) | JavaScript | 2,377 | 1 | 0 | 2,378 |
| [js/element-ui/s_index.js](/js/element-ui/index.js) | JavaScript | 25,778 | 0 | 75 | 25,853 |
| [js/s_index.js](/js/index.js) | JavaScript | 544 | 85 | 69 | 698 |
| [js/jQuery/jquery-1.10.0.js](/js/jQuery/jquery-1.10.0.js) | JavaScript | 6,632 | 1,621 | 1,548 | 9,801 |
| [js/jQuery/jquery-1.11.1.js](/js/jQuery/jquery-1.11.1.js) | JavaScript | 6,838 | 1,718 | 1,754 | 10,310 |
| [js/jQuery/jquery-3.1.1.js](/js/jQuery/jquery-3.1.1.js) | JavaScript | 6,608 | 1,686 | 1,927 | 10,221 |
| [js/jQuery/jquery-validate.js](/js/jQuery/jquery-validate.js) | JavaScript | 217 | 20 | 31 | 268 |
| [js/jQuery/jquery.easing.1.3.min.js](/js/jQuery/jquery.easing.1.3.min.js) | JavaScript | 1 | 68 | 0 | 69 |
| [js/jQuery/jquery.validate-1.13.1.js](/js/jQuery/jquery.validate-1.13.1.js) | JavaScript | 1,140 | 101 | 124 | 1,365 |
| [js/jQuery/style.css](/js/jQuery/style.css) | CSS | 29 | 0 | 6 | 35 |
| [js/list.js](/js/list.js) | JavaScript | 72 | 8 | 9 | 89 |
| [js/login.js](/js/login.js) | JavaScript | 51 | 0 | 9 | 60 |
| [js/message.js](/js/message.js) | JavaScript | 84 | 4 | 14 | 102 |
| [js/my_shop.js](/js/my_shop.js) | JavaScript | 96 | 10 | 13 | 119 |
| [js/register.js](/js/register.js) | JavaScript | 34 | 0 | 5 | 39 |
| [js/vue.js](/js/vue.js) | JavaScript | 9,892 | 1,403 | 1,053 | 12,348 |
| [js/vue@2.6.11.js](/js/vue@2.6.11.js) | JavaScript | 3,800 | 5 | 242 | 4,047 |
| [list.html](/list.html) | HTML | 2,370 | 27 | 221 | 2,618 |
| [login.html](/login.html) | HTML | 174 | 7 | 9 | 190 |
| [message.html](/message.html) | HTML | 3,981 | 45 | 191 | 4,217 |
| [my_shop.html](/my_shop.html) | HTML | 1,244 | 19 | 26 | 1,289 |
| [register.html](/register.html) | HTML | 99 | 7 | 8 | 114 |

[summary](results.md)
