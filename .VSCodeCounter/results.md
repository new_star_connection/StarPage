# Summary

Date : 2020-06-28 20:40:54

Directory d:\tomcat\webapps\starpage

Total : 43 files,  103938 codes, 8574 comments, 11867 blanks, all 124379 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript | 19 | 64,882 | 7,044 | 6,951 | 78,877 |
| CSS | 12 | 22,394 | 107 | 4,093 | 26,594 |
| HTML | 9 | 16,459 | 1,423 | 797 | 18,679 |
| XML | 1 | 153 | 0 | 1 | 154 |
| Markdown | 2 | 50 | 0 | 25 | 75 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 43 | 103,938 | 8,574 | 11,867 | 124,379 |
| css | 11 | 22,365 | 107 | 4,087 | 26,559 |
| css\theme-chalk | 1 | 13,043 | 0 | 2,811 | 15,854 |
| font | 1 | 153 | 0 | 1 | 154 |
| js | 20 | 64,911 | 7,044 | 6,957 | 78,912 |
| js\element-ui | 1 | 25,778 | 0 | 75 | 25,853 |
| js\jQuery | 7 | 21,465 | 5,214 | 5,390 | 32,069 |

[details](details.md)