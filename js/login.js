document.write("<script src='./vue@2.6.11.js'></script>");
document.write("<script src='./axios.min.js'></script>");

axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios


$(function() {
    $(".more").click(function() {
        $(this).toggleClass("active");
        $(".h").toggleClass("hide");
    });

    $(".tabLeft").click(function() {
        $(".l2").hide();
        $(".l1").show();
        $(".tabRight a").removeClass("cur");
        $(".tabLeft a").addClass("cur");
    });

    $(".tabRight").click(function() {
        $(".l1").hide();
        $(".l2").show();
        $(".tabLeft a").removeClass("cur");
        $(".tabRight a").addClass("cur");
    });
});
new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        userInfo: {
            username: '',
            password: '',
            // roleName: '',
        },
    },
    methods: {
        async submit() {
            const loading = this.$loading({
                lock: true,
                text: '登录中Loading',
                spinner: 'el-icon-loading',
                background: 'rgba(0, 0, 0, 0.7)'
            });
            setTimeout(async () => {
                const {
                    data: res
                } = await this.$http.post('person/generator/user/login', this.userInfo);
                console.log(res);
                if (res.code == 0) {
                    console.log('登录成功!');
                    this.$message.success('登录成功!');
                    sessionStorage.setItem('username', this.userInfo.username);
                    window.location.href = `./index.html`;
                } else {
                    console.log('登录失败!');
                    this.$message.error('登录失败!' + res.msg + ",正在审核角色");
                }
                loading.close();
            }, 2000);
        },
        created() {

        },

    },
})