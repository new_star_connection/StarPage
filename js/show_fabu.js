$(function() {
    $('.right-li2').mouseover(function() {
        $('.right-dropdown').show()
        $('.right-div1').css({
            'background-color': '#FFFFFF',
            border: '1px solid #E6E6E6',
            'border-bottom': 'none',
        })
        $('.right-div1 a').css({
            color: '#E3101E',
        })
    })
    $('.right-li2').mouseout(function() {
            $('.right-dropdown').hide()
            $('.right-div1').css({
                'background-color': 'transparent',
                border: '1px solid transparent',
                'border-bottom': 'none',
            })
            $('.right-div1 a').css({
                color: '#5E5E5E',
            })
        })
        //	会员俱乐部
    $('.g').each(function() {
            $(this).mouseover(function() {
                $(this).find('.navList').show()
                $(this).children('a').css({
                    'background-color': '#FFFFFF',
                    border: '1px solid #E6E6E6',
                    'border-bottom': 'none',
                })
                $(this).children('a').css({
                    color: '#E3101E',
                })
                $(this).children('a').children('i').css({
                    'background-position': '-12px 0',
                })
            })
            $(this).mouseout(function() {
                $(this).find('.navList').hide()
                $(this).children('a').css({
                    'background-color': 'transparent',
                    border: '1px solid transparent',
                    'border-bottom': 'none',
                })
                $(this).children('a').css({
                    color: '#FFFFFF',
                })
                $(this).children('a').children('i').css({
                    'background-position': '0 0',
                })
            })
        })
        //	会员俱乐部

    //	头像
    $('.cont-top-headphoto').mouseover(function() {
        $('.edit').stop().animate({
                opacity: '1',
            },
            500
        )
    })
    $('.cont-top-headphoto').mouseout(function() {
            $('.edit').stop().animate({
                    opacity: '0',
                },
                500
            )
        })
        //	头像

    //showMoreBtn
    $('.arow').click(function() {
        $(this).toggleClass('showMoreBtn')
        $(this).toggleClass('hideBtn')
        $('.hideBtn').html('收起')
        $('.showMoreBtn').html('更多类型')
        $('.dn').toggle()
    })

    //showMoreBtn

    //订单下拉列表
    $('.dindan').each(function() {
            var _this = $(this)
            $(this).click(function() {
                _this.siblings('.c-list-value').toggle()
            })
        })
        //订单下拉列表

    //猜你喜欢轮播图
    var bannerPage = 1
    var bannerLeft
    $('.bigData_btn_left').click(function() {
        bannerPage--
        if (bannerPage == 0) {
            bannerLeft = -1900
            bannerPage = 3
        } else {
            bannerLeft = -(bannerPage - 1) * 950
        }

        $('.order-like-content ul')
            .stop()
            .animate({
                    left: bannerLeft + 'px',
                },
                500
            )
    })

    $('.bigData_btn_right').click(function() {
        bannerPage++
        if (bannerPage == 4) {
            bannerLeft = 0
            bannerPage = 1
        } else {
            bannerLeft = -(bannerPage - 1) * 950
        }

        $('.order-like-content ul')
            .stop()
            .animate({
                    left: bannerLeft + 'px',
                },
                500
            )
    })

    //猜你喜欢轮播图

    $('.l-slider li').mouseover(function() {
        $('.l-slider li').removeClass('active')
        $(this).addClass('active')
    })
    $('.l-slider li').mouseout(function() {
        $('.l-slider li').removeClass('active')
        $('.current').addClass('active')
    })

    $('.attrV ul li a').each(function() {
        var _this = $(this)
        $(this).click(function() {
            $('.attrV ul li a').removeClass('cur')
            _this.addClass('cur')
        })
    })
})

function getUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        return (c === 'x' ? (Math.random() * 16) | 0 : 'r&0x3' | '0x8').toString(16)
    })
}

axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios

var vm = new Vue({
    el: '#app_shop',
    data: {
        dataObj: {
            policy: '',
            signature: '',
            key: '',
            ossaccessKeyId: '',
            dir: '',
            host: '',
        },
        dialogVisible: false,
        userInfo: {
            username: '',
            password: '',
            roleName: '',
        },
        goodsTypeList: [],
        goods: {},
        goodstype: [],
        type: '',
        RecommendUserGoodsList: [],
        orderNum: '',
        dataForm: {
            goodsNumber: '',
            typeNumber: 0,
            petName: '',
            beizhu: '',
            petAge: 0,
            status: 1,
            sort: 0,
            url: '',
            address: '',
            mianyi: 0,
            xxaddress: '',
            userTelephone: '',
        },
        dataRule: {
            petName: [{
                required: true,
                message: '宠物名不能为空',
                trigger: 'blur',
            }, ],
            url: [{
                required: true,
                message: '宠物图片url地址不能为空',
                trigger: 'blur',
            }, ],
            beizhu: [{
                required: true,
                message: '备注不能为空',
                trigger: 'blur',
            }, ],
            address: [{
                required: true,
                message: '地区不能为空',
                trigger: 'blur',
            }, ],
            showStatus: [{
                required: true,
                message: '显示状态[0-不显示；1-显示]不能为空',
                trigger: 'blur',
            }, ],
            sort: [{
                validator: (rule, value, callback) => {
                    if (value == '') {
                        callback(new Error('排序字段必须填写'))
                    } else if (!Number.isInteger(value) || value < 0) {
                        callback(new Error('排序必须是一个大于等于0的整数'))
                    } else {
                        callback()
                    }
                },
                trigger: 'blur',
            }, ],
        },
        // 图片list
        fileList: [],
        showfilelist: false,
        dialog: false,
        loading: false,
        // 获取的宠物list
        userPetList: [],
        editForm: {
            goodsNumber: '',
            typeNumber: 0,
            petName: '',
            beizhu: '',
            status: 1,
            sort: 0,
            url: '',
        },
        visible: false,
    },
    methods: {
        // 拿到许可证
        policy() {
            return new Promise(async(resolve, reject) => {
                console.log('到了')
                const { data: data } = await this.$http.get('/third/oss/policy')
                console.log(data)
                resolve(data)
            })
        },
        handleRemove(file, fileList) {
            console.log(file, fileList)
        },
        handlePreview(file) {
            console.log('handlePreview')
            console.log(file)
            this.dialogVisible = true
        },
        beforeUpload(file) {
            const _self = this
            return new Promise((resolve, reject) => {
                console.log('得到了，上传前')
                this.policy()
                    .then((response) => {
                        _self.dataObj.policy = response.data.policy
                        _self.dataObj.signature = response.data.signature
                        _self.dataObj.ossaccessKeyId = response.data.accessid
                        _self.dataObj.key = response.data.dir + getUUID() + '_${filename}'
                        _self.dataObj.dir = response.data.dir
                        _self.dataObj.host = response.data.host
                        console.log(_self.dataObj)
                        console.log('成功~')
                        resolve(true)
                    })
                    .catch((err) => {
                        reject(false)
                    })
            })
        },
        successUpload(response, file) {
            // this.fileList.pop()
            this.showfilelist = true
            console.log('successUpload')
                // console.log(this.fileList);
                // this.fileList.push({
                //   name: file.name,
                //   url: this.dataObj.host + '/' + this.dataObj.key.replace('${filename}', file.name)
                // })
                // console.log(this.fileList);
            console.log(
                this.dataObj.host +
                '/' +
                this.dataObj.key.replace('${filename}', file.name)
            )
            this.dataForm.url =
                this.dataObj.host +
                '/' +
                this.dataObj.key.replace('${filename}', file.name)
            this.editForm.url = this.dataForm.url
                // console.log(file);
        },
        // 提交
        onSubmit() {
            console.log('submit!')
        },
        async init() {
            var url = window.location.href //获取当前url
            var dz_url = url.split('#')[0] //获取#/之前的字符串
            var cs = dz_url.split('?')[1] //获取?之后的参数字符串
            console.log(cs)
            if (cs == undefined) return
            var cs_arr = cs.split('&') //参数字符串分割为数组
            var cs = {}
            console.log(cs_arr)
            for (var i = 0; i < cs_arr.length; i++) {
                //遍历数组，拿到json对象
                cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
            }
            this.userInfo.username = cs.username //这样就拿到了参数中的数据
            console.log(this.userInfo.username)
            this.fileList = null
                // 去拿到这个人的身份
            const { data: res } = await this.$http.get(
                `/person/generator/userrole/user_role_info/${this.userInfo.username}`
            )
            res.userRoleList.forEach(async(element) => {
                // 去查roleName
                const { data: res1 } = await this.$http.get(
                    `person/generator/role/info/${element.roleNumber}`
                )
                if (res1.role.roleName == '发布者') {
                    this.userInfo.roleName = '发布者'
                }
            })
        },
        async getGoods() {
            const { data: res1 } = await this.$http.get(
                `goods/generator/petentity/info/1272710263098843138`
            )

            console.log(res1)
            this.goods = res1.petEntity
            const { data: res2 } = await this.$http.get(
                `goods/generator/pettype/info/tree/${this.goods.typeNumber}`
            )
            console.log(res2)
            this.goodstype = res2.data
            for (var i = 0; i < this.goodstype.length; i++) {
                if (this.goodstype[i].typeNumber == this.goods.typeNumber) {
                    this.type = this.goodstype[i].typeName
                    console.log(this.goodstype[i].typeName)
                }
            }
            console.log(this.goods)
        },
        MyCat() {
            window.location.href = `./GoWuChe.html?username=${this.userInfo.username}`
        },
        ReturnFirstPage() {
            window.location.href = `./index.html?username=${this.userInfo.username}`
        },
        JupmPage(item) {
            window.location.href =
                `./` + item + `.html?username=${this.userInfo.username}`
        },
        //得到用户推荐
        async getRecommendUserList() {
            if (
                this.userInfo.username != '' &&
                this.userInfo.username != null &&
                this.userInfo.username != undefined
            ) {
                // 查找usermapping
                const { data: res1 } = await this.$http.get(
                    `person/generator/user/info/${this.userInfo.username}`
                )
                console.log('用户信息!')
                console.log(res1.user)
                const { data: res } = await this.$http.get(
                    `recommend/generator/recommond/goods/${res1.user.usernameMapping}`
                )
                console.log('推荐内容!')
                console.log(res)
                if (res.recommondUser != null) {
                    // 使用goods_mapping 去查询goods相关信息
                    res.recommondUser.recommendations.forEach(async(element) => {
                        const { data: res } = await this.$http.get(
                            `goods/generator/petentity/goods_mapping/${element.goods_mapping}`
                        )
                        this.RecommendUserGoodsList.push({
                            url: res.goods.url,
                            score: element.score,
                            goodsName: res.goods.petName,
                            beizhu: res.goods.beizhu,
                            goodsNumber: res.goods.goodsNumber,
                        })
                    })
                } else {
                    // 大众推荐
                    const { data: res } = await this.$http.get(
                            `recommend/generator/recommond/over`
                        )
                        // console.log("大众推荐内容!");
                        // console.log(res);
                        // 使用goods_mapping 去查询goods相关信息
                    res.overList.forEach(async(element) => {
                        const { data: res } = await this.$http.get(
                            `goods/generator/petentity/goods_mapping/${element.goods_mapping}`
                        )
                        this.RecommendUserGoodsList.push({
                            url: res.goods.url,
                            count: element.count,
                            goodsName: res.goods.petName,
                            beizhu: res.goods.beizhu,
                            goodsNumber: res.goods.goodsNumber,
                        })
                    })
                }
                // console.log(this.RecommendUserGoodsList.length);
                console.log('推荐内容----------->')
                console.log(this.RecommendUserGoodsList)
            } else {
                // 大众推荐
                const { data: res } = await this.$http.get(
                        `recommend/generator/recommond/over`
                    )
                    // console.log("大众推荐内容!");
                console.log(res)
                    // 使用goods_mapping 去查询goods相关信息
                res.overList.forEach(async(element) => {
                    const { data: res } = await this.$http.get(
                        `goods/generator/petentity/goods_mapping/${element.goods_mapping}`
                    )
                    this.RecommendUserGoodsList.push({
                        url: res.goods.url,
                        count: element.count,
                        goodsName: res.goods.petName,
                        beizhu: res.goods.beizhu,
                        goodsNumber: res.goods.goodsNumber,
                    })
                })
                console.log('大众推荐内容!')
                console.log(this.RecommendUserGoodsList)
            }
        },
        //提交发布表单
        async dataFormSubmit() {
            console.log('----开始提交----')
            console.log(this.dataForm)
            this.dialog = false
            const { data: res } = await this.$http.post(
                `goods/generator/petentity/save`, {
                    petName: this.dataForm.petName,
                    petAge: this.dataForm.petAge,
                    mianyi: this.mianyi,
                    person: this.userInfo.username,
                    address: this.dataForm.address,
                    xxaddress: this.dataForm.xxaddress,
                    userTelephone: this.dataForm.userTelephone,
                    checkFlat: 0,
                    ly: 0,
                    status: 1,
                    beizhu: this.dataForm.beizhu,
                    url: this.dataForm.url,
                }
            )
            if (res.code == 0) {
                console.log(res)
                this.$message.success('发布成功!')
            } else {
                this.$message.error('发布失败,' + res.msg)
            }
        },
        handleClose() {
            if (this.loading) {
                return
            }
            this.$confirm('确定要提交表单吗？')
                .then((_) => {
                    this.loading = true
                    this.timer = setTimeout(() => {
                            // 动画关闭需要一定的时间
                            setTimeout(() => {
                                this.loading = false
                                this.dialog = false
                                    // done();
                                    // 完成提交
                                this.dataFormSubmit()
                            }, 400)
                        }, 2000)
                        // 刷新
                        // this.getuserPetList();
                })
                .catch((_) => {})
        },
        cancelForm() {
            this.loading = false
            this.dialog = false
            clearTimeout(this.timer)
        },
        async getuserPetList() {
            const { data: res } = await this.$http.post(
                    `goods/generator/petentity/username/${this.userInfo.username}`
                )
                // this.userPetList = res.goodsList;
            console.log(res)
            this.userPetList = []
            res.goodsList.forEach((element) => {
                if (element.status !== 0) {
                    this.userPetList.push(element)
                }
            })
            if (res.code == 0) {
                this.$message.success('成功获取信息!')
            }
            console.log('获取到用户发布信息')
            console.log(this.userPetList)
        },
        async addOrUpdateHandle(goodsNumber) {
            // 编译框
            this.visible = true
            const { data: res } = await this.$http.get(
                `/goods/generator/petentity/info/${goodsNumber}`
            )
            console.log('更新~~~')
            console.log(res)
            this.editForm = res.petEntity
            this.showfilelist = true
            console.log(this.editForm)
        },
        async editSumbit() {
            const { data: res } = await this.$http.post(
                `/goods/generator/petentity/update`, {
                    petName: this.editForm.petName,
                    address: this.editForm.address,
                    goodsNumber: this.editForm.goodsNumber,
                    url: this.editForm.url,
                    beizhu: this.editForm.beizhu,
                }
            )
            this.visible = false
            console.log('确认更新')
            console.log(res)
            if (res.code == 0) {
                this.$message.success('更新成功~')
            } else {
                this.$message.error('更新失败~,' + res.msg)
            }
            // 刷新
            this.getuserPetList()
        },
        async deleteHandle(goodsNumber) {
            this.$confirm(`确定对[id=${goodsNumber}]进行删除操作?`, '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
            }).then(async() => {
                // console.log("daole")
                const { data: res } = await this.$http.post(
                    `/goods/generator/petentity/delete/${goodsNumber}`
                )
                console.log('确认删除')
                console.log(res)
                if (res.code == 0) {
                    this.$message.success('删除成功!')
                } else {
                    this.$message.error('删除失败!' + res.msg)
                }
                this.getuserPetList()
            })
        },
        gotoPet(goodsNumber) {
            window.location.href = `./message.html?username=${this.userInfo.username}&goodsNumber=${goodsNumber}`
        },
    },
    created() {
        this.init()
        this.getGoods()
        this.getRecommendUserList()
        this.getuserPetList()
    },
})