$(function() {
    $(window).scroll(function() {
        var top = $(document).scrollTop();
        if (top > 600) {
            $("#filter-box").addClass("onfixed");
        } else {
            $("#filter-box").removeClass("onfixed");
        }
    });

    //	brand
    $(".b1").click(function() {
        $(".b1").toggleClass("brand-more");
        $(".b1").toggleClass("close-brand");
        $(".brand-more").html("<i></i>更多");
        $(".close-brand").html("<i></i>收起");
        $(".close-brand i").css({
            "background-position": "3px -219px",
        });
        $(".hide").toggle();
    });

    $(".b2").click(function() {
        $(".b2").toggleClass("brand-more");
        $(".b2").toggleClass("close-brand");
        $(".brand-more").html("<i></i>展开");
        $(".close-brand").html("<i></i>收起");
        $(".close-brand i").css({
            "background-position": "3px -219px",
        });
        $(".h").toggle();
    });

    $(".c-brand").each(function() {
        var _this = $(this);
        var h = $(this).find("a").text();
        h = $.trim(h);
        $(this).mouseover(function() {
            _this.css({
                "border": "1px solid #e3101e",
            });
            _this.find("a").css({
                "background": "#FFFFFF url()",
                "color": "#e3101e",
                "text-indent": "0px",
            });
        });
        $(this).mouseout(function() {
            _this.css({
                "border": "1px solid #f2f2f2",
            });
            _this.find("a").css({
                "background": "url(./images/list/1) no-repeat 9px 9px",
                "text-indent": "-999px",
            });
        });

    });

    /*	$(".category-syn-tit").each(function(){
    		var _this = $(this);
    		$(this).mouseover(function(){
    			$(".category-syn-con").hide();
    			_this.siblings("dd").show();
    		});
    	});*/

    $(".category-syn").each(function() {
        var _this = $(this);
        $(this).mouseover(function() {
            _this.addClass("category-syn-open");
            $(".category-syn-con").hide();
            _this.find(".category-syn-con").show();
        });
        $(this).mouseout(function() {
            $(".category-syn-con").hide();
            _this.removeClass("category-syn-open");
        });
    });

    $(".facet").each(function() {
        var _this = $(this);
        _this.click(function() {
            _this.addClass("chk");
        });
    });

});



axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios;


var vm = new Vue({
    el: '#app_list',
    data: {
        message: 'Hello Vue!',
        userInfo: {
            username: '',
            password: '',
            roleName: '',
        },
        recommandGoosList2: [],
        goodsTypeList: [],
        goodsList: [],
        goodsListAll: [],
        numberRecommod: 4,
        recommandGoosList: [],
        LunboList: [],
        LunboNumber: 5,
        falt: 1,
        pageIndex: 1,
        pageSize: 12,
        totalPage: 0,
        key: '',
        // 购物车list
        UserCarList: [],
        RecommendUserGoodsList: [],
    },
    methods: {
        // 下一页
        nex() {
            this.pageIndex = this.pageIndex + 1;
            // 访问数据
            this.getGoodsListAll();
        },
        // 上一页
        prev() {
            this.pageIndex = this.pageIndex - 1;
            if (this.pageIndex < 1) {
                this.pageIndex = 1;
            }
            // 访问数据
            this.getGoodsListAll();
        },
        init() {
            // var url = window.location.href; //获取当前url
            // var dz_url = url.split('#')[0]; //获取#/之前的字符串
            // var cs = dz_url.split('?')[1]; //获取?之后的参数字符串
            // console.log(cs)
            // if (cs == undefined) return;
            // var cs_arr = cs.split('&'); //参数字符串分割为数组
            // var cs = {};
            // console.log(cs_arr);
            // for (var i = 0; i < cs_arr.length; i++) { //遍历数组，拿到json对象
            //   cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
            // }
            let username = sessionStorage['username'];
            let keyname = sessionStorage['search_key'];
            this.userInfo.username = username; //这样就拿到了参数中的数据
            this.key = keyname;
            console.log("username: " + this.userInfo.username);
            console.log("key: " + this.key);
        },
        async getMenu() {
            const {
                data: res
            } = await this.$http.get(`goods/generator/pettype/list/tree`);
            console.log(res);
            this.goodsTypeList = res.data;
            console.log(this.goodsTypeList);
        },
        MyCat() {
            window.location.href = `./GoWuChe.html?username=${this.userInfo.username}`;
        },
        async getUserCar() {
            if (this.userInfo.username != "" && this.userInfo.username != null && this.userInfo.username != undefined) {
                const {
                    data: res
                } =
                await this.$http.get(
                    `person/generator/shoppingcar/carListUser/${this.userInfo.username}`);
                console.log("-----------得到用户购物车----------");
                console.log(res.shoppingList);
                this.UserCarList = [];
                res.shoppingList.forEach(async(element) => {
                    // 去查询一个商品的url
                    const {
                        data: res
                    } = await this.$http.get(`goods/generator/petentity/info/${element.goodsNumber}`)
                    if (res.petEntity.status !== 0) {
                        // 如果被下架了不加入
                        this.UserCarList.push({
                            url: res.petEntity.url,
                            goodsName: res.petEntity.petName
                        });
                    }
                });
                console.log('购物车信息');
                console.log(this.UserCarList);
            }
        },
        async getGoodsList() {
            const {
                data: res
            } = await this.$http.get(`goods/generator/petentity/recommand/${this.numberRecommod}`);
            console.log("------这是一个商品的list----");
            console.log(res);
            // this.goodsTypeList = res.data;
            // console.log(this.goodsTypeList);
            this.recommandGoosList = res.entityList;
            console.log(this.recommandGoosList);
        },
        async getLunBoList() {
            const {
                data: res
            } = await this.$http.post(`goods/generator/petentity/lunboGoods/${this.LunboNumber}`);
            console.log(res);
            if (res.code === 0) {
                this.LunboList = res.petEntityList;
            } else {
                this.falt = 0;
                this.$message.error('轮播数据获取失败!');
            }
            console.log(this.LunboList);
            console.log(this.falt);
        },
        async getGoodsListAll() {
            const {
                data: res
            } = await this.$http.post(`goods/generator/petentity/list`, {
                "page": this.pageIndex,
                "limit": this.pageSize,
                "key": this.key
            });
            console.log("------这是一个页面的20个商品的list----");
            console.log(res);
            // this.goodsTypeList = res.data;
            // console.log(this.goodsTypeList);
            this.goodsListAll = res.page.list;
            console.log(this.goodsListAll);
        },
        async insertShappingCar(username, goods) {
            if (username == '' || username == undefined || username == null) {
                // 弹出去往登录界面的框
                this.$alert('未登录，请进入登录界面进行登录', '提示', {
                    confirmButtonText: '确定',
                    callback: action => {
                        this.$message({
                            type: 'info',
                            message: `action: ${ action }`
                        });
                        window.location.href = './login.html';
                    }
                });
            }
            // 用户名
            // 商品id
            console.log("添加到购物车");
            console.log(username + "," + goods);
            const {
                data: res
            } = await this.$http.post(`person/generator/shoppingcar/save`, {
                username: username,
                goodsNumber: goods,
                num: 1
            });
            console.log(res);
            if (res.code == 0) {
                this.$message({
                    message: '添加到购物车成功!',
                    type: 'success'
                });
            } else {
                this.$message.error('添加到购物车失败!' + res.msg);
            }
            // 刷新
            this.getUserCar();
        },
        async insertCollect(username, goods) {
            if (username == '' || username == undefined || username == null) {
                // 弹出去往登录界面的框
                this.$alert('未登录，请进入登录界面进行登录', '提示', {
                    confirmButtonText: '确定',
                    callback: action => {
                        this.$message({
                            type: 'info',
                            message: `action: ${ action }`
                        });
                        window.location.href = './login.html';
                    }
                });
                return;
            }
            console.log("添加到收藏夹");
            console.log(username + "," + goods);
            // this.$message.success(username);
            const {
                data: res
            } = await this.$http.post(`/person/generator/usercollect/save`, {
                username: username,
                goodsNumber: goods
            });
            console.log(res);
            if (res.code == 0) {
                this.$message.success('添加到收藏夹成功!');
            } else {
                this.$message.error("添加到收藏夹失败!" + res.msg);
            }


        },
        submit1() {
            this.$message.success('zhse dasda');
        },
        async getRecommendUserList() {
            if (this.userInfo.username != "" && this.userInfo.username != null && this.userInfo.username != undefined) {
                // 查找usermapping
                const {
                    data: res1
                } = await this.$http.get(`person/generator/user/info/${this.userInfo.username}`);
                console.log("用户信息!");
                console.log(res1.user);
                const {
                    data: res
                } = await this.$http.get(`recommend/generator/recommond/goods/${res1.user.usernameMapping}`);
                console.log("推荐内容!");
                console.log(res);
                if (res.recommondUser != null) {
                    // 使用goods_mapping 去查询goods相关信息
                    res.recommondUser.recommendations.forEach(async(element) => {
                        const {
                            data: res
                        } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                        if (res.goods.status == 1) {
                            this.RecommendUserGoodsList.push({
                                url: res.goods.url,
                                score: element.score,
                                goodsName: res.goods.petName,
                                beizhu: res.goods.beizhu,
                                goodsNumber: res.goods.goodsNumber
                            });
                        }
                    });
                } else {
                    // 大众推荐
                    const {
                        data: res
                    } = await this.$http.get(`recommend/generator/recommond/over`);
                    // console.log("大众推荐内容!");
                    // console.log(res);
                    // 使用goods_mapping 去查询goods相关信息
                    res.overList.forEach(async(element) => {
                        const {
                            data: res
                        } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                        if (res.goods.status == 1) {
                            this.RecommendUserGoodsList.push({
                                url: res.goods.url,
                                score: element.score,
                                goodsName: res.goods.petName,
                                beizhu: res.goods.beizhu,
                                goodsNumber: res.goods.goodsNumber
                            });
                        }
                    });
                }
                // console.log(this.RecommendUserGoodsList.length);
                console.log('推荐内容----------->');
                console.log(this.RecommendUserGoodsList);
            } else {
                // 大众推荐
                const {
                    data: res
                } = await this.$http.get(`recommend/generator/recommond/over`);
                // console.log("大众推荐内容!");
                console.log(res);
                // 使用goods_mapping 去查询goods相关信息
                res.overList.forEach(async(element) => {
                    const {
                        data: res
                    } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                    if (res.goods.status == 1) {
                        this.RecommendUserGoodsList.push({
                            url: res.goods.url,
                            score: element.score,
                            goodsName: res.goods.petName,
                            beizhu: res.goods.beizhu,
                            goodsNumber: res.goods.goodsNumber
                        });
                    }
                });
                console.log("大众推荐内容!");
                console.log(this.RecommendUserGoodsList);
            }
        },
        async search() {
            // const {
            //   data: res
            // } = await this.$http.post(`goods/generator/petentity/list`, {
            //   page: this.pageIndex,
            //   limit: this.pageSize,
            //   key: this.key
            // });
            // console.log("查询结果--->");
            // console.log(res);
            // this.getGoodsListAll = res.page.list;
            // console.log(this.getGoodsListAll);
            this.getGoodsListAll();
        },
        gotoPet(goodsNumber) {
            sessionStorage.setItem('username', this.userInfo.username);
            sessionStorage.setItem('goodsNumber', goodsNumber);
            window.location.href = `./message.html`;
        },
        search_item(key) {
            this.key = key;
            this.search();
        },
        personZX() {
            window.location.href = `./shop_me.html?username=${this.userInfo.username}`;
        },
        ReturnFirstPage() {
            window.location.href = `./index.html?username=${this.userInfo.username}`;
        },
        async getAllRecommend() {
            const {
                data: res
            } = await this.$http.get('recommend/generator/recommond/goods');
            console.log("----------asdas-----");
            console.log(res);
            // this.recommandGoosList2 = res.recommondList;
            res.recommondList.forEach(async(element) => {
                element.recommendations.forEach(async(element) => {
                    const {
                        data: res1
                    } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                    this.recommandGoosList2.push({
                        name: res1.goods.petName,
                        beizhu: res1.goods.beizhu,
                        url: res1.goods.url,
                        goodsNumber: res1.goods.goodsNumber
                    });
                });
            });
            console.log("这是所有用户的推荐宠物!");
            console.log(this.recommandGoosList2);
        },
        searchGoods(name) {
            this.key = name;
            this.search();
        }
    },
    created() {
        // 页面加载完毕
        this.init();
        this.getMenu();
        this.getGoodsList();
        this.getLunBoList();
        this.getUserCar();
        this.getGoodsListAll();
        this.getAllRecommend();
        // this.submit1();
        this.getRecommendUserList();
    },
    // 这里运行js代码
    updated() {
        // loading();
    },
})