var c = 0;

function next() {
    if (c < 2) {
        c++;
        left = c * (-240);
        left = left + 'px';
        $("#ul-icon").animate({
            "left": left
        });
    }
}


function prev() {
    if (c > 0) {
        c--;
        left = c * (-240);
        left = left + 'px';
        $("#ul-icon").animate({
            "left": left
        });
    }
}

function loading() {
    $(function () {
        $(".left-li2").mouseover(function () {
            $(".drop-down").show();
            $(".left-div1").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".left-div1 a").css({
                "color": "#E3101E",
            });
        });
        $(".left-li2").mouseout(function () {
            $(".drop-down").hide();
            $(".left-div1").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".left-div1 a").css({
                "color": "#5E5E5E",
            });
        });

        $(".right-li2").mouseover(function () {
            $(".right-dropdown").show();
            $(".right-div1").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".right-div1 a").css({
                "color": "#E3101E",
            });
        });
        $(".right-li2").mouseout(function () {
            $(".right-dropdown").hide();
            $(".right-div1").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".right-div1 a").css({
                "color": "#5E5E5E",
            });
        });

        $(".service").mouseover(function () {
            $(".s-dropdown").show();
            $(".s-div").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".s-div a").css({
                "color": "#E3101E",
            });
        });
        $(".service").mouseout(function () {
            $(".s-dropdown").hide();
            $(".s-div").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".s-div a").css({
                "color": "#5E5E5E",
            });
        });

        $(".nav").mouseover(function () {
            $(".n-dropdown").show();
            $(".n-div").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".n-div").css({
                "color": "#E3101E",
            });
        });
        $(".nav").mouseout(function () {
            $(".n-dropdown").hide();
            $(".n-div").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".n-div").css({
                "color": "#5E5E5E",
            });
        });

        $(".phone").mouseover(function () {
            $(".p-dropdown").show();
            $(".ph-div").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".ph-div a").css({
                "color": "#E3101E",
            });
        });
        $(".phone").mouseout(function () {
            $(".p-dropdown").hide();
            $(".ph-div").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".ph-div a").css({
                "color": "#5E5E5E",
            });
        });

        //	处理右边列表

        $(".nav-item").each(function (e) {
            //e为0-14
            var n = parseInt(e) + 1;
            var _this = $(this);
            $(this).mouseover(function () {
                $(this).addClass('white');
                $(this).find("h3").children().css({
                    "color": "#333333"
                });
                $(".l" + n).addClass('show');
            });
            $(".l" + n).mouseover(function () {
                _this.addClass('white');
                _this.find("h3").children().css({
                    "color": "#333333"
                });
                $(".l" + n).addClass('show');
            });

            $(this).mouseout(function () {
                $(this).removeClass('white');
                $(this).find("h3").children().css({
                    "color": "#FFFFFF"
                });
                $(".l" + n).removeClass('show');
            });
            $(".l" + n).mouseout(function () {
                _this.removeClass('white');
                _this.find("h3").children().css({
                    "color": "#FFFFFF"
                });
                $(".l" + n).removeClass('show');
            });
        });

        //focus
        $(".nav-o li").each(function (e) {
            var n = parseInt(e) + 1;
            var _this = $(this);
            $(this).mouseover(function () {
                $('.banner' + n).fadeIn().siblings().hide();
                _this.find("a").css({
                    "background-color": "red",
                    "opacity": "1",
                });
            });
            $(this).mouseout(function () {
                _this.find("a").css({
                    "background-color": "#000",
                    "opacity": ".3",
                });
            });
        });
        //focus

        //op
        $(".op").each(function (e) {
            var _this = $(this);
            $(this).mouseover(function () {
                _this.stop().animate({
                    "opacity": "0.8"
                }, 500, function () {
                    _this.stop().animate({
                        "opacity": "1"
                    }, 500);
                });
            });

        });
        //op

        //slider
        $(".no li").each(function (e) {
            var n = parseInt(e) + 1;
            var _this = $(this);
            $(this).mouseover(function () {
                $('.slider' + n).fadeIn().siblings().hide();
                _this.find("a").css({
                    "background-color": "red",
                    "opacity": "1",
                });
            });
            $(this).mouseout(function () {
                _this.find("a").css({
                    "background-color": "#fff",
                    "opacity": ".6",
                });
            });
        });
        //	猜你喜欢
    });
}

function loading2() {
    /**************************/
    /*banner*/

    var nowpage = 1;
    var time;

    time = setInterval(run, 2000);

    $(".focus-box").mouseover(function () {
        clearInterval(time);
    });
    $(".nav-o").mouseover(function () {
        clearInterval(time);
    });
    $(".cover1").mouseover(function () {
        clearInterval(time);
    });
    $(".cover2").mouseover(function () {
        clearInterval(time);
    });

    $(".focus-box").mouseout(function () {
        time = setInterval(run, 2000);
    });
    $(".nav-o").mouseout(function () {
        time = setInterval(run, 2000);
    });
    $(".cover1").mouseout(function () {
        time = setInterval(run, 2000);
    });
    $(".cover2").mouseout(function () {
        time = setInterval(run, 2000);
    });

    function run() {
        if (nowpage == 6) {
            nowpage = 1;
        }

        $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();

        $(".nav-o li").eq(nowpage - 1).find('a').css({
            "background-color": "red",
            "opacity": "1",
        }).parent('li').siblings(".nav-o li").find('a').css({
            "background-color": "#000",
            "opacity": ".3",
        });

        nowpage++;
    }

    $(".nav-o li").mouseover(function () {

        var _this = $(this);
        zz = setTimeout(function () {
            var index = _this.index();
            index++;
            nowpage = index;

            $(".nav-o li a").css({
                "background-color": "#000",
                "opacity": ".3"
            });
            _this.find('a').css({
                "background-color": "red",
                "opacity": "1"
            });

            $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();

        }, 300)

    });

    $(".nav-o li").mouseout(function () {
        clearTimeout(zz);
    });

    $(".cover1").click(function () {
        nowpage--;
        if (nowpage == 0) {
            nowpage = 5;
        }

        $(".nav-o li").eq(nowpage - 1).find('a').css({
            "background-color": "red",
            "opacity": "1",
        }).parent('li').siblings(".nav-o li").find('a').css({
            "background-color": "#000",
            "opacity": ".3",
        });

        $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();

    });

    $(".cover2").click(function () {

        if (nowpage == 6) {
            nowpage = 1;
        }

        $(".nav-o li").eq(nowpage - 1).find('a').css({
            "background-color": "red",
            "opacity": "1",
        }).parent('li').siblings(".nav-o li").find('a').css({
            "background-color": "#000",
            "opacity": ".3",
        });

        $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();
        nowpage++;
    });

    /*****************************/

    //slider

    //tab
    $(".tab li").each(function (e) {
        var n = parseInt(e) + 1;
        var _this = $(this);
        $(this).mouseover(function () {
            $('.main' + n).fadeIn().siblings().hide();
            _this.addClass('cur').siblings().removeClass('cur');
        });
        $(this).mouseout(function () {

        });
    });
    //tab

    //ad
    $(".close").click(function () {
        $(".ad-div").slideUp();
    });
    //ad

    //	倒计时

    function GetRTime() {
        var EndTime = new Date('2020/9/5 22:00:00');
        var NowTime = new Date();
        var t = EndTime.getTime() - NowTime.getTime();
        var d = 0;
        var h = 0;
        var m = 0;
        var s = 0;
        if (t >= 0) {
            //    d=Math.floor(t/1000/60/60/24);
            h = Math.floor(t / 1000 / 60 / 60 % 24);
            m = Math.floor(t / 1000 / 60 % 60);
            s = Math.floor(t / 1000 % 60);
        }

        /*document.getElementById("t_d").innerHTML = d + "天";
        document.getElementById("t_h").innerHTML = h + "时";
        document.getElementById("t_m").innerHTML = m + "分";
        document.getElementById("t_s").innerHTML = s + "秒";*/

        $(".num").eq(0).html(h);
        $(".num").eq(1).html(m);
        $(".num").eq(2).html(s);

    }

    setInterval(GetRTime, 0);

    //	倒计时

    //	猜你喜欢
    var guessPage = 1;
    $(".nex").click(function () {
        if (guessPage == 4) {
            guessPage = 1;
        }
        $(".ul").hide();
        $(".ul" + guessPage).show();
        guessPage++;
    });
    $(".prev").click(function () {
        if (guessPage == 0) {
            guessPage = 3;
        }
        $(".ul").hide();
        $(".ul" + guessPage).show();
        guessPage--;
    });
}


axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios;


var vm = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        userInfo: {
            username: '',
            password: '',
            roleName: '',
        },
        /**
         * 秒杀商品列表
         */
        spikeList: [],
        /**
         * 普通商品列表
         */
        goodsList: [],
        goodsPage: {
            pageIndex: 1,
            pageSize: 20,
            totalPage: 0,
        },
        /**
         * 热门商品列表
         */
        popularList: [],
        /**
         * 商品分类tree
         */
        goodsTypeList: [],
        key: "",
        recommandGoosList2: [],
        UserCarList: [],
        RecommendUserGoodsList: [],
        LunboList: [],
        MessageInfoList: [],


    },
    methods: {
        async init() {
            let username = sessionStorage['username'];
            this.userInfo.username = username;
            //获取到秒杀商品
            const {data: res1} = await this.$http.get("/goods/goods_entity/spike/list");
            console.log("--------秒杀商品-------");
            console.log(res1.spikelist);
            this.spikeList = res1.spikelist;
            //获取到普通商品列表
            const {data: res2} = await this.$http.post("/goods/goods_entity/list", {
                page: this.goodsPage.pageIndex,
                limit: this.goodsPage.pageSize,
                key: ""
            });
            console.log("--------普通商品-------");
            console.log(res2.page.list);
            this.goodsList = res2.page.list;
            this.goodsPage.pageIndex = res2.page.currPage;
            this.goodsPage.totalPage = res2.page.totalPage;
            this.goodsPage.pageSize = res2.page.pageSize;
            //获取热门商品
            const {data: res3} = await this.$http.get("/goods/goods_entity/popular/list");
            console.log("--------热门商品-------");
            console.log(res3.popularlist);
            this.popularList = res3.popularlist;
        },
        personZX() {
            if (this.userInfo.username === '' ||
                this.userInfo.username == null) {
                this.$message.error("未登录！请登录后才能进入个人中心!");
                window.location.href = './login.html';
            } else {
                //跳转到个人中心
                sessionStorage.setItem('url_type', 'gr');
                sessionStorage.setItem('username', this.userInfo.username);
                sessionStorage.setItem('status','1');
                window.location.href = `./person.html`;
            }

        },
        search() {

        },
        s_gotoPet(goodsNumber) {

        },
        s_searchGoods(name) {

        },
        s_insertCollect() {

        },
        /**
         * 添加商品到购物车
         * @param goodsId
         * @returns {Promise<void>}
         */
        async s_insertShoppingCar(goodsId) {
            let username = this.userInfo.username;
            if (username === '' || username === undefined || username == null) {
                // 弹出去往登录界面的框
                this.$alert('未登录，请进入登录界面进行登录', '提示', {
                    confirmButtonText: '确定',
                    callback: action => {
                        this.$message({
                            type: 'info',
                            message: `action: ${action}`
                        });
                        window.location.href = './login.html';
                    }
                });
                return;
            }
            const {data: res} = await this.$http.post("/person/generator/shoppingcar/save", {
                spcarId: "",
                username: this.userInfo.username,
                goodsNumber: goodsId,
                num: 1,
                status: 1
            });
            if (res.code === 0) {
                this.$message.success('添加商品Id={' + goodsId + '}到购物车成功');
                this.s_getShoppingCart();
            } else {
                this.$message.error('添加商品Id={' + goodsId + '}到购物车失败,' + res.msg);
            }
        },
        /**
         * 获取用户购物车信息
         * @returns {Promise<void>}
         */
        async s_getShoppingCart() {
            if (this.userInfo.username !== "" && this.userInfo.username !== null && this.userInfo.username !== undefined) {
                let status = 1;
                const {
                    data: res
                } =
                    await this.$http.get(
                        `person/generator/shoppingcar/carListUser/${this.userInfo.username}/${status}`);
                this.UserCarList = [];
                res.shoppingList.forEach(async (element) => {
                    // 去查询一个商品的url
                    const {
                        data: res
                    } = await this.$http.get(`goods/goods_entity/info/${element.goodsNumber}`)
                    if (res.goodsEntity.status !== 0) {
                        // 如果被下架了不加入
                        this.UserCarList.push({
                            goodsUrl: res.goodsEntity.goodsUrl,
                            goodsName: res.goodsEntity.goodsName
                        });
                    }
                });
                console.log('购物车信息');
                console.log(this.UserCarList);
            }
        },
        /**
         * 获取普通商品页面
         */
        async getGoodsList() {
            //获取到普通商品列表
            const {data: res2} = await this.$http.post("/goods/goods_entity/list", {
                page: this.goodsPage.pageIndex,
                limit: this.goodsPage.pageSize,
                key: ""
            });
            console.log("--------普通商品-------");
            console.log(res2.page.list);
            this.goodsList = res2.page.list;
            this.goodsPage.pageIndex = res2.page.currPage;
            this.goodsPage.totalPage = res2.page.totalPage;
            this.goodsPage.pageSize = res2.page.pageSize;
        },
        /**
         * 得到所有的分类信息
         * @returns {Promise<void>}
         */
        async getMenu() {
            const {
                data: res
            } = await this.$http.get(`goods/goods_type/list/tree`);
            console.log(res);
            this.goodsTypeList = res.data;
            console.log(this.goodsTypeList);
        },
        /**
         * 上一页
         */
        s_prev() {
            console.log(this.goodsPage.pageIndex);
            if (this.goodsPage.pageIndex === 1) {
                return;
            }
            this.goodsPage.pageIndex = (this.goodsPage.pageIndex) - 1;
            this.getGoodsList();
        },
        /**
         * 下一页
         */
        s_nex() {
            console.log(this.goodsPage.pageIndex);
            console.log(this.goodsPage.totalPage);
            if (this.goodsPage.pageIndex >= this.goodsPage.totalPage) {
                return;
            }
            this.goodsPage.pageIndex = this.goodsPage.pageIndex + 1;
            this.getGoodsList();
        },
        /**
         * 获取轮播商品
         * @returns {Promise<void>}
         */
        async getLunBoList() {
            const {data: res} = await this.$http.get("/goods/goods_entity/lunbo/list");
            this.LunboList = res.lunbolist;
        },
        s_MyCat(){
            sessionStorage.setItem("username", this.userInfo.username);
            this.status = 1;
            sessionStorage.setItem("status",this.status);
            window.location.href = `./GoWuChe.html`;
        }
    },
    created() {
        this.getMenu();
        this.getLunBoList();
        this.init();
        this.s_getShoppingCart();
    },
    // 这里运行js代码
    updated() {
        loading();
    },
    mounted() {
        loading2();
    },

    beforeDestroy() {
    },
})
