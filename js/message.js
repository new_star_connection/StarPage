$(function() {
    $(".on").mouseover(function() {
        $(".navBox").show();
    });
    $(".on").mouseout(function() {
        $(".navBox").hide();
    });

    $(".navBox").mouseover(function() {
        $(".navBox").show();
    });
    $(".navBox").mouseout(function() {
        $(".navBox").hide();
    });
    //分类结束

    $(".breadcrumb").mouseover(function() {
        $(".has-breads").css({
            "border-bottom": "transparent",
        });
        $(".details-breadcrumbs-dropdown").show();
    });
    $(".breadcrumb").mouseout(function() {
        $(".has-breads").css({
            "border": "1px solid #ccc",
        });
        $(".details-breadcrumbs-dropdown").hide();
    });

    //放大镜
    $(".jqzoom").mouseover(function() {
        $(".zoomdiv").show();
    });
    $(".jqzoom").mouseout(function() {
        $(".zoomdiv").hide();
    });

    $(".fdj li").each(function() {
        var _this = $(this);
        $(this).mouseover(function() {
            //			alert(_this.index()+1);
            var num = _this.index() + 1;
            $(".jqzoom img").attr("src", "images/message/" + num + ".jpg");
            $(".zoomdiv img").attr("src", "images/message/" + num + "_big.jpg");
        });
    });

    $(".pager ol li").each(function() {
        var _this = $(this);
        var left = _this.index() * (-180);
        $(this).mouseover(function() {
            $(".carousel ul").stop().animate({
                "left": left + "px",
            });
            $(".pager ol li").removeClass("active");
            $(".pager ol li").eq(_this.index()).addClass("active");
        });
    });

    $(".prdtitbox li").each(function(e) {
        var _this = $(this);
        $(this).click(function() {
            $(".prdtitbox li").removeClass("cur");
            _this.addClass("cur");
            $(".l").hide();
            $(".l").eq(e).show();
        });
    });

    $(".prdcol").click(function() {
        $(".prdcol a").removeClass("select");
        $(this).find("a").addClass("select");
    });

    $(".prdmod").click(function() {
        $(".prdmod a").removeClass("select");
        $(this).find("a").addClass("select");
    });

    $(".plus").click(function() {
        //		alert($("#count").attr("value"));
        var v = $("#count").attr("value");
        var maxlength = $("#count").attr("maxlength");
        if (v < maxlength) {
            v++;
            $("#count").attr("value", v);
        }

    });

    $(".minus").click(function() {
        var v = $("#count").attr("value");
        var minlength = 1;
        if (v > minlength) {
            v--;
            $("#count").attr("value", v);
        }

    });

});

function loading() {
    //放大镜
    $(".jqzoom").mouseover(function() {
        $(".zoomdiv").show();
    });
    $(".jqzoom").mouseout(function() {
        $(".zoomdiv").hide();
    });



    $(".left-li2").mouseover(function() {
        $(".drop-down").show();
        $(".left-div1").css({
            "background-color": "#FFFFFF",
            "border": "1px solid #E6E6E6",
            "border-bottom": "none",
        });
        $(".left-div1 a").css({
            "color": "#E3101E",
        });
    });
    $(".left-li2").mouseout(function() {
        $(".drop-down").hide();
        $(".left-div1").css({
            "background-color": "transparent",
            "border": "1px solid transparent",
            "border-bottom": "none",
        });
        $(".left-div1 a").css({
            "color": "#5E5E5E",
        });
    });

    $(".right-li2").mouseover(function() {
        $(".right-dropdown").show();
        $(".right-div1").css({
            "background-color": "#FFFFFF",
            "border": "1px solid #E6E6E6",
            "border-bottom": "none",
        });
        $(".right-div1 a").css({
            "color": "#E3101E",
        });
    });
    $(".right-li2").mouseout(function() {
        $(".right-dropdown").hide();
        $(".right-div1").css({
            "background-color": "transparent",
            "border": "1px solid transparent",
            "border-bottom": "none",
        });
        $(".right-div1 a").css({
            "color": "#5E5E5E",
        });
    });

    $(".service").mouseover(function() {
        $(".s-dropdown").show();
        $(".s-div").css({
            "background-color": "#FFFFFF",
            "border": "1px solid #E6E6E6",
            "border-bottom": "none",
        });
        $(".s-div a").css({
            "color": "#E3101E",
        });
    });
    $(".service").mouseout(function() {
        $(".s-dropdown").hide();
        $(".s-div").css({
            "background-color": "transparent",
            "border": "1px solid transparent",
            "border-bottom": "none",
        });
        $(".s-div a").css({
            "color": "#5E5E5E",
        });
    });

    $(".nav").mouseover(function() {
        $(".n-dropdown").show();
        $(".n-div").css({
            "background-color": "#FFFFFF",
            "border": "1px solid #E6E6E6",
            "border-bottom": "none",
        });
        $(".n-div").css({
            "color": "#E3101E",
        });
    });
    $(".nav").mouseout(function() {
        $(".n-dropdown").hide();
        $(".n-div").css({
            "background-color": "transparent",
            "border": "1px solid transparent",
            "border-bottom": "none",
        });
        $(".n-div").css({
            "color": "#5E5E5E",
        });
    });

    $(".phone").mouseover(function() {
        $(".p-dropdown").show();
        $(".ph-div").css({
            "background-color": "#FFFFFF",
            "border": "1px solid #E6E6E6",
            "border-bottom": "none",
        });
        $(".ph-div a").css({
            "color": "#E3101E",
        });
    });
    $(".phone").mouseout(function() {
        $(".p-dropdown").hide();
        $(".ph-div").css({
            "background-color": "transparent",
            "border": "1px solid transparent",
            "border-bottom": "none",
        });
        $(".ph-div a").css({
            "color": "#5E5E5E",
        });
    });

    //	处理右边列表

    $(".nav-item").each(function(e) {
        //e为0-14
        var n = parseInt(e) + 1;
        var _this = $(this);
        $(this).mouseover(function() {
            $(this).addClass('white');
            $(this).find("h3").children().css({
                "color": "#333333"
            });
            $(".l" + n).addClass('show');
        });
        $(".l" + n).mouseover(function() {
            _this.addClass('white');
            _this.find("h3").children().css({
                "color": "#333333"
            });
            $(".l" + n).addClass('show');
        });

        $(this).mouseout(function() {
            $(this).removeClass('white');
            $(this).find("h3").children().css({
                "color": "#FFFFFF"
            });
            $(".l" + n).removeClass('show');
        });
        $(".l" + n).mouseout(function() {
            _this.removeClass('white');
            _this.find("h3").children().css({
                "color": "#FFFFFF"
            });
            $(".l" + n).removeClass('show');
        });
    });

    //focus
    $(".nav-o li").each(function(e) {
        var n = parseInt(e) + 1;
        var _this = $(this);
        $(this).mouseover(function() {
            $('.banner' + n).fadeIn().siblings().hide();
            _this.find("a").css({
                "background-color": "red",
                "opacity": "1",
            });
        });
        $(this).mouseout(function() {
            _this.find("a").css({
                "background-color": "#000",
                "opacity": ".3",
            });
        });
    });
    //focus

    //op
    $(".op").each(function(e) {
        var _this = $(this);
        $(this).mouseover(function() {
            _this.stop().animate({
                "opacity": "0.8"
            }, 500, function() {
                _this.stop().animate({
                    "opacity": "1"
                }, 500);
            });
        });

    });
    //op

    //slider
    $(".no li").each(function(e) {
        var n = parseInt(e) + 1;
        var _this = $(this);
        $(this).mouseover(function() {
            $('.slider' + n).fadeIn().siblings().hide();
            _this.find("a").css({
                "background-color": "red",
                "opacity": "1",
            });
        });
        $(this).mouseout(function() {
            _this.find("a").css({
                "background-color": "#fff",
                "opacity": ".6",
            });
        });
    });

    /*var nowpage = 1;
              var time;
              time = setInterval(run, 1000);
  
              $(".focus-box").mouseover(function() {
              	clearInterval(time);
              	$(".pre").show();
              	$(".next").show();
              });
              $(".focus-box").mouseout(function() {
              	time = setInterval(run, 1000);
              	$(".pre").hide();
              	$(".next").hide();
              });
  
              function run(){
              	if(nowpage == 6){
              		nowpage = 1;
              	}
              	$(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();
              	
              	$(".no li").eq(nowpage - 1).find("a").css({
              			"background-color":"red",
              			"opacity": "1",
              		}).siblings('li').css({
              			"background-color":"#fff",
              			"opacity": ".6",
              		});
              		
              	$(".nav-o li a").eq(nowpage - 1).css({
              			"background-color":"red",
              			"opacity": "1",
              		}).parent('li').siblings().find('a').css({
              			"background-color":"#000",
              			"opacity": ".6",
              		});
              	
              	nowpage++;
              }*/

    /**************************/
    /*banner*/

    var nowpage = 1;
    var time;

    time = setInterval(run, 2000);

    $(".focus-box").mouseover(function() {
        clearInterval(time);
    });
    $(".nav-o").mouseover(function() {
        clearInterval(time);
    });
    $(".cover1").mouseover(function() {
        clearInterval(time);
    });
    $(".cover2").mouseover(function() {
        clearInterval(time);
    });

    $(".focus-box").mouseout(function() {
        time = setInterval(run, 2000);
    });
    $(".nav-o").mouseout(function() {
        time = setInterval(run, 2000);
    });
    $(".cover1").mouseout(function() {
        time = setInterval(run, 2000);
    });
    $(".cover2").mouseout(function() {
        time = setInterval(run, 2000);
    });

    function run() {
        if (nowpage == 6) {
            nowpage = 1;
        }

        $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();

        $(".nav-o li").eq(nowpage - 1).find('a').css({
            "background-color": "red",
            "opacity": "1",
        }).parent('li').siblings(".nav-o li").find('a').css({
            "background-color": "#000",
            "opacity": ".3",
        });

        nowpage++;
    }

    $(".nav-o li").mouseover(function() {

        var _this = $(this);
        zz = setTimeout(function() {
            var index = _this.index();
            index++;
            nowpage = index;

            $(".nav-o li a").css({
                "background-color": "#000",
                "opacity": ".3"
            });
            _this.find('a').css({
                "background-color": "red",
                "opacity": "1"
            });

            $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();

        }, 300)

    });

    $(".nav-o li").mouseout(function() {
        clearTimeout(zz);
    });

    $(".cover1").click(function() {
        nowpage--;
        if (nowpage == 0) {
            nowpage = 5;
        }

        $(".nav-o li").eq(nowpage - 1).find('a').css({
            "background-color": "red",
            "opacity": "1",
        }).parent('li').siblings(".nav-o li").find('a').css({
            "background-color": "#000",
            "opacity": ".3",
        });

        $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();

    });

    $(".cover2").click(function() {

        if (nowpage == 6) {
            nowpage = 1;
        }

        $(".nav-o li").eq(nowpage - 1).find('a').css({
            "background-color": "red",
            "opacity": "1",
        }).parent('li').siblings(".nav-o li").find('a').css({
            "background-color": "#000",
            "opacity": ".3",
        });

        $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();
        nowpage++;
    });

    /*****************************/

    //slider

    //tab
    $(".tab li").each(function(e) {
        var n = parseInt(e) + 1;
        var _this = $(this);
        $(this).mouseover(function() {
            $('.main' + n).fadeIn().siblings().hide();
            _this.addClass('cur').siblings().removeClass('cur');
        });
        $(this).mouseout(function() {

        });
    });
    //tab

    //ad
    $(".close").click(function() {
        $(".ad-div").slideUp();
    });
    //ad

    //	倒计时

    function GetRTime() {
        var EndTime = new Date('2020/06/31 22:00:00');
        var NowTime = new Date();
        var t = EndTime.getTime() - NowTime.getTime();
        var d = 0;
        var h = 0;
        var m = 0;
        var s = 0;
        if (t >= 0) {
            //    d=Math.floor(t/1000/60/60/24);
            h = Math.floor(t / 1000 / 60 / 60 % 24);
            m = Math.floor(t / 1000 / 60 % 60);
            s = Math.floor(t / 1000 % 60);
        }

        /*document.getElementById("t_d").innerHTML = d + "天";
        document.getElementById("t_h").innerHTML = h + "时";
        document.getElementById("t_m").innerHTML = m + "分";
        document.getElementById("t_s").innerHTML = s + "秒";*/

        $(".num").eq(0).html(h);
        $(".num").eq(1).html(m);
        $(".num").eq(2).html(s);

    }
    setInterval(GetRTime, 0);

    //	倒计时

    //	猜你喜欢
    var guessPage = 1;
    $(".nex").click(function() {
        if (guessPage == 4) {
            guessPage = 1;
        }
        $(".ul").hide();
        $(".ul" + guessPage).show();
        guessPage++;
    });
    $(".prev").click(function() {
        if (guessPage == 0) {
            guessPage = 3;
        }
        $(".ul").hide();
        $(".ul" + guessPage).show();
        guessPage--;
    });


    //	猜你喜欢	
}


axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios;



var vm = new Vue({
    el: '#app_message',
    data: {
        userInfo: {
            username: '',
            password: '',
            roleName: '',
        },
        goodsInfo: {
            goodsNumber: '',
        },
        goodsTypeList: [],
        goods: {},
        goodstype: [],
        type: '',
        UserCarList: [],
        recommandGoosList2: [],
        orderInfo: {
            orderNum: '',
            goodsPrice: 0,
            goodsNums: 0,
            goodsUrl: '',
            goodsNumber: ''
        },
        key: '',
        goodsListAll: []
    },
    methods: {
        searchGoods(name) {
            this.key = name;
            this.search();
        },
        personZX() {
            window.location.href = `./shop_me.html?username=${this.userInfo.username}`;
        },
        init() {
            // 以前使用url拼凑，现在转为session 保存数据传输
            // var url = window.location.href; //获取当前url
            // var dz_url = url.split('#')[0]; //获取#/之前的字符串
            // var cs = dz_url.split('?')[1]; //获取?之后的参数字符串
            // console.log(cs)
            // if (cs == undefined) return;
            // var cs_arr = cs.split('&'); //参数字符串分割为数组
            // var cs = {};
            // console.log(cs_arr);
            // for (var i = 0; i < cs_arr.length; i++) { //遍历数组，拿到json对象
            //   cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
            // }

            this.userInfo.username = sessionStorage["username"]; //这样就拿到了参数中的数据
            this.goodsInfo.goodsNumber = sessionStorage["goodsNumber"];
            console.log("用户名称：" + this.userInfo.username);
            console.log("宠物编号:" + this.goodsInfo.goodsNumber);
        },
        async getGoods() {
            const {
                data: res1
            } = await this.$http.get(`goods/generator/petentity/info/${this.goodsInfo.goodsNumber}`);
            console.log("查询到的宠物信息~");
            console.log(res1);
            this.goods = res1.petEntity;
            const {
                data: res2
            } = await this.$http.get(`goods/generator/pettype/info/tree/${this.goods.typeNumber}`);
            console.log(res2);
            this.goodstype = res2.data;
            for (var i = 0; i < this.goodstype.length; i++) {
                if (this.goodstype[i].typeNumber == this.goods.typeNumber) {
                    this.type = this.goodstype[i].typeName;
                    console.log(this.goodstype[i].typeName);
                }
            }
            console.log(this.goods);
        },
        async getUserCar() {
            const {
                data: res
            } =
            await this.$http.get(
                `person/generator/shoppingcar/carListUser/${this.userInfo.username}`);
            console.log(res.shoppingList);
            this.UserCarList = [];
            res.shoppingList.forEach(async(element) => {
                // 去查询一个商品的url
                const {
                    data: res
                } = await this.$http.get(`goods/generator/petentity/info/${element.goodsNumber}`)
                if (res.petEntity.status !== 0) {
                    // 如果被下架了不加入
                    this.UserCarList.push({
                        url: res.petEntity.url,
                        goodsName: res.petEntity.petName
                    });
                }
            });
            console.log('购物车信息');
            console.log(this.UserCarList);
        },
        async insertShappingCar(username, goods) {
            if (username == '' || username == undefined || username == null) {
                // 弹出去往登录界面的框
                this.$alert('未登录，请进入登录界面进行登录', '提示', {
                    confirmButtonText: '确定',
                    callback: action => {
                        this.$message({
                            type: 'info',
                            message: `action: ${ action }`
                        });
                        window.location.href = './login.html';
                    }
                });
                return;
            }
            // 用户名
            // 商品id
            console.log("添加到购物车");
            console.log(username + "," + goods);
            const {
                data: res
            } = await this.$http.post(`person/generator/shoppingcar/save`, {
                username: username,
                goodsNumber: goods,
                num: 1
            });
            console.log(res);
            if (res.code == 0) {
                this.$message({
                    message: '添加到购物车成功!',
                    type: 'success'
                });
            } else {
                this.$message.error('添加到购物车失败!' + res.msg);
            }
            // 刷新
            this.getUserCar();
        },
        async insertCollect(username, goods) {
            if (username == '' || username == undefined || username == null) {
                // 弹出去往登录界面的框
                this.$alert('未登录，请进入登录界面进行登录', '提示', {
                    confirmButtonText: '确定',
                    callback: action => {
                        this.$message({
                            type: 'info',
                            message: `action: ${ action }`
                        });
                        window.location.href = './login.html';
                    }
                });
                return;
            }
            console.log("添加到收藏夹");
            console.log(username + "," + goods);
            // this.$message.success(username);
            const {
                data: res
            } = await this.$http.post(`/person/generator/usercollect/save`, {
                username: username,
                goodsNumber: goods
            });
            console.log(res);
            if (res.code == 0) {
                this.$message.success('添加到收藏夹成功!');
            } else {
                this.$message.error("添加到收藏夹失败!" + res.msg);
            }
        },
        MyCat() {
            window.location.href = `./GoWuChe.html?username=${this.userInfo.username}`;
        },
        async getMenu() {
            const {
                data: res
            } = await this.$http.get(`goods/generator/pettype/list/tree`);
            console.log("类别tree");
            console.log(res);
            this.goodsTypeList = res.data;
            console.log(this.goodsTypeList);
        },
        gotoFirstPage() {
            window.location.href = `./index.html?username=${this.userInfo.username}`;
        },
        async getAllRecommend() {
            const {
                data: res
            } = await this.$http.get('recommend/generator/recommond/goods');
            console.log("----------asdas-----");
            console.log(res);
            // this.recommandGoosList2 = res.recommondList;
            res.recommondList.forEach(async(element) => {
                element.recommendations.forEach(async(element) => {
                    const {
                        data: res1
                    } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                    this.recommandGoosList2.push({
                        name: res1.goods.petName,
                        beizhu: res1.goods.beizhu,
                        url: res1.goods.url,
                        goodsNumber: res1.goods.goodsNumber
                    });
                });
            });
            console.log("这是所有用户的推荐宠物!");
            console.log(this.recommandGoosList2);
        },
        searchGoods(name) {
            console.log(name);
            this.key = name;
            this.search();
        },
        async getGoodsListAll() {
            const {
                data: res
            } = await this.$http.post(`goods/generator/petentity/list`, {
                "page": this.pageIndex,
                "limit": this.pageSize,
                "key": this.key
            });
            console.log("------这是一个页面的20个商品的list----");
            console.log(res);
            // this.goodsTypeList = res.data;
            // console.log(this.goodsTypeList);
            this.goodsListAll = res.page.list;
            console.log(this.goodsListAll);
        },
        async search() {
            window.location.href = `./list.html`;
            // ?username=${this.userInfo.username}&key=${this.key}
            sessionStorage.setItem('username', this.userInfo.username);
            sessionStorage.setItem('search_key', this.key);
        },

    },
    created() {
        this.init();
        this.getGoods();
        this.getMenu();
        this.getUserCar();
        this.getAllRecommend();
    },
    updated() {
        loading();
    },
})

//test:  file:///F:/pet/StarPage/message.html?username=123&goods_number=1001