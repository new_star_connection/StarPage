$(function() {
    $(".right-li2").mouseover(function() {
        $(".right-dropdown").show();
        $(".right-div1").css({
            "background-color": "#FFFFFF",
            "border": "1px solid #E6E6E6",
            "border-bottom": "none",
        });
        $(".right-div1 a").css({
            "color": "#E3101E",
        });
    });
    $(".right-li2").mouseout(function() {
        $(".right-dropdown").hide();
        $(".right-div1").css({
            "background-color": "transparent",
            "border": "1px solid transparent",
            "border-bottom": "none",
        });
        $(".right-div1 a").css({
            "color": "#5E5E5E",
        });
    });
    //	会员俱乐部
    $(".g").each(function() {
        $(this).mouseover(function() {
            $(this).find(".navList").show();
            $(this).children("a").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(this).children("a").css({
                "color": "#E3101E",
            });
            $(this).children("a").children("i").css({
                "background-position": "-12px 0",
            });
        });
        $(this).mouseout(function() {
            $(this).find(".navList").hide();
            $(this).children("a").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(this).children("a").css({
                "color": "#FFFFFF",
            });
            $(this).children("a").children("i").css({
                "background-position": "0 0",
            });
        });
    });
    //	会员俱乐部

    //	头像
    $(".cont-top-headphoto").mouseover(function() {
        $(".edit").stop().animate({
            "opacity": "1",
        }, 500);
    });
    $(".cont-top-headphoto").mouseout(function() {
        $(".edit").stop().animate({
            "opacity": "0",
        }, 500);
    });
    //	头像

    //showMoreBtn
    $(".arow").click(function() {
        $(this).toggleClass("showMoreBtn");
        $(this).toggleClass("hideBtn");
        $(".hideBtn").html("收起");
        $(".showMoreBtn").html("更多类型");
        $(".dn").toggle();
    });

    //showMoreBtn

    //订单下拉列表
    $(".dindan").each(function() {
        var _this = $(this);
        $(this).click(function() {
            _this.siblings(".c-list-value").toggle();
        });
    });
    //订单下拉列表


    //猜你喜欢轮播图
    var bannerPage = 1;
    var bannerLeft;
    $(".bigData_btn_left").click(function() {
        bannerPage--;
        if (bannerPage == 0) {
            bannerLeft = -1900;
            bannerPage = 3;
        } else {
            bannerLeft = -(bannerPage - 1) * 950;
        }

        $(".order-like-content ul").stop().animate({
            "left": bannerLeft + "px",
        }, 500);
    });

    $(".bigData_btn_right").click(function() {
        bannerPage++;
        if (bannerPage == 4) {
            bannerLeft = 0;
            bannerPage = 1;
        } else {
            bannerLeft = -(bannerPage - 1) * 950;
        }

        $(".order-like-content ul").stop().animate({
            "left": bannerLeft + "px",
        }, 500);
    });

    //猜你喜欢轮播图

    $(".l-slider li").mouseover(function() {
        $(".l-slider li").removeClass("active");
        $(this).addClass("active");
    });
    $(".l-slider li").mouseout(function() {
        $(".l-slider li").removeClass("active");
        $(".current").addClass("active");
    });

    $(".attrV ul li a").each(function() {
        var _this = $(this);
        $(this).click(function() {
            $(".attrV ul li a").removeClass("cur");
            _this.addClass("cur");
        });
    });

});


axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios;



var vm = new Vue({
    el: '#app_shopme',
    data: {
        userInfo: {
            username: '',
            password: '',
            roleName: '',
            email: '',
            userTelephone: '',
        },
        person: {},
        UserCarList: [],
        RecommendUserGoodsList: [],
        editvisible: false,
    },
    methods: {
        async init() {
            // 之前使用的url拼凑，现在转为session保存信息
            // var url = window.location.href; //获取当前url
            // var dz_url = url.split('#')[0]; //获取#/之前的字符串
            // var cs = dz_url.split('?')[1]; //获取?之后的参数字符串
            // console.log(cs)
            // if (cs == undefined) return;
            // var cs_arr = cs.split('&'); //参数字符串分割为数组
            // var cs = {};
            // console.log(cs_arr);
            // for (var i = 0; i < cs_arr.length; i++) { //遍历数组，拿到json对象
            //     cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
            // }
            var user = sessionStorage['username'];
            console.log("session  ======>>>>>" + user);
            this.userInfo.username = user; //这样就拿到了参数中的数据
            // 去拿到这个人的身份
            const {
                data: res
            } = await this.$http.get(`/person/generator/userrole/user_role_info/${this.userInfo.username}`);
            res.userRoleList.forEach(async(element) => {
                // 去查roleName
                const {
                    data: res1
                } = await this.$http.get(`person/generator/role/info/${element.roleNumber}`);
                if (res1.role.roleName == "发布者") {
                    this.userInfo.roleName = "发布者";
                }
            });

        },
        async getPerson() {
            const {
                data: res
            } = await this.$http.get(`person/generator/user/info/${this.userInfo.username}`);
            console.log("sadsdadasd");
            console.log(res);
            this.userInfo = res.user;
            console.log(this.person);
        },
        MyCat() {
            window.location.href = `./GoWuChe.html?username=${this.userInfo.username}`;
        },
        async getUserCar() {
            const {
                data: res
            } =
            await this.$http.get(
                `person/generator/shoppingcar/carListUser/${this.userInfo.username}`);
            console.log("-----------得到用户购物车----------");
            console.log(res.shoppingList);
            this.UserCarList = [];
            res.shoppingList.forEach(async(element) => {
                // 去查询一个商品的url
                const {
                    data: res
                } = await this.$http.get(`goods/generator/petentity/info/${element.goodsNumber}`)
                this.UserCarList.push({
                    url: res.petEntity.url,
                    goodsName: res.petEntity.petName
                });
            });
            console.log('购物车信息');
            console.log(this.UserCarList);
        },
        ReturnFirstPage() {
            window.location.href = `./index.html?username=${this.userInfo.username}`;
        },
        JupmPage(item) {
            window.location.href = `./` + item + `.html?username=${this.userInfo.username}`;
        },
        //得到用户推荐
        async getRecommendUserList() {
            if (this.userInfo.username != "" && this.userInfo.username != null && this.userInfo.username != undefined) {
                // 查找usermapping
                const {
                    data: res1
                } = await this.$http.get(`person/generator/user/info/${this.userInfo.username}`);
                console.log("用户信息!");
                console.log(res1.user);
                const {
                    data: res
                } = await this.$http.get(`recommend/generator/recommond/goods/${res1.user.usernameMapping}`);
                console.log("推荐内容!");
                console.log(res);
                if (res.recommondUser != null) {
                    // 使用goods_mapping 去查询goods相关信息
                    res.recommondUser.recommendations.forEach(async(element) => {
                        const {
                            data: res
                        } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                        this.RecommendUserGoodsList.push({
                            url: res.goods.url,
                            score: element.score,
                            goodsName: res.goods.petName,
                            beizhu: res.goods.beizhu,
                            goodsNumber: res.goods.goodsNumber
                        });
                    });
                } else {
                    // 大众推荐
                    const {
                        data: res
                    } = await this.$http.get(`recommend/generator/recommond/over`);
                    // console.log("大众推荐内容!");
                    // console.log(res);
                    // 使用goods_mapping 去查询goods相关信息
                    res.overList.forEach(async(element) => {
                        const {
                            data: res
                        } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                        this.RecommendUserGoodsList.push({
                            url: res.goods.url,
                            count: element.count,
                            goodsName: res.goods.petName,
                            beizhu: res.goods.beizhu,
                            goodsNumber: res.goods.goodsNumber
                        });
                    });
                }
                // console.log(this.RecommendUserGoodsList.length);
                console.log('推荐内容----------->');
                console.log(this.RecommendUserGoodsList);
            } else {
                // 大众推荐
                const {
                    data: res
                } = await this.$http.get(`recommend/generator/recommond/over`);
                // console.log("大众推荐内容!");
                console.log(res);
                // 使用goods_mapping 去查询goods相关信息
                res.overList.forEach(async(element) => {
                    const {
                        data: res
                    } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                    this.RecommendUserGoodsList.push({
                        url: res.goods.url,
                        count: element.count,
                        goodsName: res.goods.petName,
                        beizhu: res.goods.beizhu,
                        goodsNumber: res.goods.goodsNumber
                    });
                });
                console.log("大众推荐内容!");
                console.log(this.RecommendUserGoodsList);
            }
        },
        gotoPet(goodsNumber) {
            window.location.href = `./message.html?username=${this.userInfo.username}&goodsNumber=${goodsNumber}`;
        },
        edit() {
            this.editvisible = true;
        },
        async editSumbit() {
            const {
                data: res
            } = await this.$http.post(`/person/generator/user/update`, this.userInfo);
            if (res.code == 0) {
                this.$message.success("修改成功");
                this.init();
            } else {
                this.$message.error("修改失败!" + res.msg)
            }
            this.editvisible = false;
        }
    },
    created() {
        this.init();
        this.getPerson();
        this.getUserCar();
        this.getRecommendUserList();
    },
})

//file:///F:/pet/StarPage/shop_me.html?username=123