var c = 0;

function next() {
    if (c < 2) {
        c++;
        left = c * (-240);
        left = left + 'px';
        $("#ul-icon").animate({
            "left": left
        });
    }
}


function prev() {
    if (c > 0) {
        c--;
        left = c * (-240);
        left = left + 'px';
        $("#ul-icon").animate({
            "left": left
        });
    }
}

function loading() {
    $(function () {
        $(".left-li2").mouseover(function () {
            $(".drop-down").show();
            $(".left-div1").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".left-div1 a").css({
                "color": "#E3101E",
            });
        });
        $(".left-li2").mouseout(function () {
            $(".drop-down").hide();
            $(".left-div1").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".left-div1 a").css({
                "color": "#5E5E5E",
            });
        });

        $(".right-li2").mouseover(function () {
            $(".right-dropdown").show();
            $(".right-div1").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".right-div1 a").css({
                "color": "#E3101E",
            });
        });
        $(".right-li2").mouseout(function () {
            $(".right-dropdown").hide();
            $(".right-div1").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".right-div1 a").css({
                "color": "#5E5E5E",
            });
        });

        $(".service").mouseover(function () {
            $(".s-dropdown").show();
            $(".s-div").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".s-div a").css({
                "color": "#E3101E",
            });
        });
        $(".service").mouseout(function () {
            $(".s-dropdown").hide();
            $(".s-div").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".s-div a").css({
                "color": "#5E5E5E",
            });
        });

        $(".nav").mouseover(function () {
            $(".n-dropdown").show();
            $(".n-div").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".n-div").css({
                "color": "#E3101E",
            });
        });
        $(".nav").mouseout(function () {
            $(".n-dropdown").hide();
            $(".n-div").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".n-div").css({
                "color": "#5E5E5E",
            });
        });

        $(".phone").mouseover(function () {
            $(".p-dropdown").show();
            $(".ph-div").css({
                "background-color": "#FFFFFF",
                "border": "1px solid #E6E6E6",
                "border-bottom": "none",
            });
            $(".ph-div a").css({
                "color": "#E3101E",
            });
        });
        $(".phone").mouseout(function () {
            $(".p-dropdown").hide();
            $(".ph-div").css({
                "background-color": "transparent",
                "border": "1px solid transparent",
                "border-bottom": "none",
            });
            $(".ph-div a").css({
                "color": "#5E5E5E",
            });
        });

        //	处理右边列表

        $(".nav-item").each(function (e) {
            //e为0-14
            var n = parseInt(e) + 1;
            var _this = $(this);
            $(this).mouseover(function () {
                $(this).addClass('white');
                $(this).find("h3").children().css({
                    "color": "#333333"
                });
                $(".l" + n).addClass('show');
            });
            $(".l" + n).mouseover(function () {
                _this.addClass('white');
                _this.find("h3").children().css({
                    "color": "#333333"
                });
                $(".l" + n).addClass('show');
            });

            $(this).mouseout(function () {
                $(this).removeClass('white');
                $(this).find("h3").children().css({
                    "color": "#FFFFFF"
                });
                $(".l" + n).removeClass('show');
            });
            $(".l" + n).mouseout(function () {
                _this.removeClass('white');
                _this.find("h3").children().css({
                    "color": "#FFFFFF"
                });
                $(".l" + n).removeClass('show');
            });
        });

        //focus
        $(".nav-o li").each(function (e) {
            var n = parseInt(e) + 1;
            var _this = $(this);
            $(this).mouseover(function () {
                $('.banner' + n).fadeIn().siblings().hide();
                _this.find("a").css({
                    "background-color": "red",
                    "opacity": "1",
                });
            });
            $(this).mouseout(function () {
                _this.find("a").css({
                    "background-color": "#000",
                    "opacity": ".3",
                });
            });
        });
        //focus

        //op
        $(".op").each(function (e) {
            var _this = $(this);
            $(this).mouseover(function () {
                _this.stop().animate({
                    "opacity": "0.8"
                }, 500, function () {
                    _this.stop().animate({
                        "opacity": "1"
                    }, 500);
                });
            });

        });
        //op

        //slider
        $(".no li").each(function (e) {
            var n = parseInt(e) + 1;
            var _this = $(this);
            $(this).mouseover(function () {
                $('.slider' + n).fadeIn().siblings().hide();
                _this.find("a").css({
                    "background-color": "red",
                    "opacity": "1",
                });
            });
            $(this).mouseout(function () {
                _this.find("a").css({
                    "background-color": "#fff",
                    "opacity": ".6",
                });
            });
        });
        //	猜你喜欢
    });
}

function loading2() {
    /**************************/
    /*banner*/

    var nowpage = 1;
    var time;

    time = setInterval(run, 2000);

    $(".focus-box").mouseover(function () {
        clearInterval(time);
    });
    $(".nav-o").mouseover(function () {
        clearInterval(time);
    });
    $(".cover1").mouseover(function () {
        clearInterval(time);
    });
    $(".cover2").mouseover(function () {
        clearInterval(time);
    });

    $(".focus-box").mouseout(function () {
        time = setInterval(run, 2000);
    });
    $(".nav-o").mouseout(function () {
        time = setInterval(run, 2000);
    });
    $(".cover1").mouseout(function () {
        time = setInterval(run, 2000);
    });
    $(".cover2").mouseout(function () {
        time = setInterval(run, 2000);
    });

    function run() {
        if (nowpage == 6) {
            nowpage = 1;
        }

        $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();

        $(".nav-o li").eq(nowpage - 1).find('a').css({
            "background-color": "red",
            "opacity": "1",
        }).parent('li').siblings(".nav-o li").find('a').css({
            "background-color": "#000",
            "opacity": ".3",
        });

        nowpage++;
    }

    $(".nav-o li").mouseover(function () {

        var _this = $(this);
        zz = setTimeout(function () {
            var index = _this.index();
            index++;
            nowpage = index;

            $(".nav-o li a").css({
                "background-color": "#000",
                "opacity": ".3"
            });
            _this.find('a').css({
                "background-color": "red",
                "opacity": "1"
            });

            $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();

        }, 300)

    });

    $(".nav-o li").mouseout(function () {
        clearTimeout(zz);
    });

    $(".cover1").click(function () {
        nowpage--;
        if (nowpage == 0) {
            nowpage = 5;
        }

        $(".nav-o li").eq(nowpage - 1).find('a').css({
            "background-color": "red",
            "opacity": "1",
        }).parent('li').siblings(".nav-o li").find('a').css({
            "background-color": "#000",
            "opacity": ".3",
        });

        $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();

    });

    $(".cover2").click(function () {

        if (nowpage == 6) {
            nowpage = 1;
        }

        $(".nav-o li").eq(nowpage - 1).find('a').css({
            "background-color": "red",
            "opacity": "1",
        }).parent('li').siblings(".nav-o li").find('a').css({
            "background-color": "#000",
            "opacity": ".3",
        });

        $(".focus-box li").eq(nowpage - 1).stop().show().siblings('li').stop().hide();
        nowpage++;
    });

    /*****************************/

    //slider

    //tab
    $(".tab li").each(function (e) {
        var n = parseInt(e) + 1;
        var _this = $(this);
        $(this).mouseover(function () {
            $('.main' + n).fadeIn().siblings().hide();
            _this.addClass('cur').siblings().removeClass('cur');
        });
        $(this).mouseout(function () {

        });
    });
    //tab

    //ad
    $(".close").click(function () {
        $(".ad-div").slideUp();
    });
    //ad

    //	倒计时

    function GetRTime() {
        var EndTime = new Date('2020/7/5 22:00:00');
        var NowTime = new Date();
        var t = EndTime.getTime() - NowTime.getTime();
        var d = 0;
        var h = 0;
        var m = 0;
        var s = 0;
        if (t >= 0) {
            //    d=Math.floor(t/1000/60/60/24);
            h = Math.floor(t / 1000 / 60 / 60 % 24);
            m = Math.floor(t / 1000 / 60 % 60);
            s = Math.floor(t / 1000 % 60);
        }

        /*document.getElementById("t_d").innerHTML = d + "天";
        document.getElementById("t_h").innerHTML = h + "时";
        document.getElementById("t_m").innerHTML = m + "分";
        document.getElementById("t_s").innerHTML = s + "秒";*/

        $(".num").eq(0).html(h);
        $(".num").eq(1).html(m);
        $(".num").eq(2).html(s);

    }

    setInterval(GetRTime, 0);

    //	倒计时

    //	猜你喜欢
    var guessPage = 1;
    $(".nex").click(function () {
        if (guessPage == 4) {
            guessPage = 1;
        }
        $(".ul").hide();
        $(".ul" + guessPage).show();
        guessPage++;
    });
    $(".prev").click(function () {
        if (guessPage == 0) {
            guessPage = 3;
        }
        $(".ul").hide();
        $(".ul" + guessPage).show();
        guessPage--;
    });
}


axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios;


var vm = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        userInfo: {
            username: '',
            password: '',
            roleName: '',
        },
        goodsTypeList: [],
        goodsList: [],
        goodsListAll: [],
        numberRecommod: 4,
        // recommandGoosList: [],
        recommandGoosList2: [],
        LunboList: [],
        LunboNumber: 5,
        falt: 1,
        pageIndex: 1,
        pageSize: 12,
        totalPage: 0,
        key: '',
        // 购物车list
        UserCarList: [],
        RecommendUserGoodsList: [],
        Message: {
            pageIndex: 1,
            pageSize: 10,
        },
        MessageInfoList: [],
        petRecomList2: [],
        status:0
    },
    methods: {
        // 下一页
        nex() {
            this.pageIndex = this.pageIndex + 1;
            // 访问数据
            this.getGoodsListAll();
        },
        // 上一页
        prev() {
            this.pageIndex = this.pageIndex - 1;
            if (this.pageIndex < 1) {
                this.pageIndex = 1;
            }
            // 访问数据
            this.getGoodsListAll();
        },
        init() {
            //以前使用url拼凑  现在转为session
            // var url = window.location.href; //获取当前url
            // var dz_url = url.split('#')[0]; //获取#/之前的字符串
            // var cs = dz_url.split('?')[1]; //获取?之后的参数字符串
            // console.log(cs)
            // if (cs == undefined) return;
            // var cs_arr = cs.split('&'); //参数字符串分割为数组
            // var cs = {};
            // console.log(cs_arr);
            // for (var i = 0; i < cs_arr.length; i++) { //遍历数组，拿到json对象
            //     cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
            // }
            this.userInfo.username = sessionStorage['username']; //这样就拿到了参数中的数据
            console.log(this.userInfo.username);
        },
        /**
         * 得到所有的分类信息
         * @returns {Promise<void>}
         */
        async getMenu() {
            const {
                data: res
            } = await this.$http.get(`goods/generator/pettype/list/tree`);
            console.log(res);
            this.goodsTypeList = res.data;
            console.log(this.goodsTypeList);
        },
        /**
         * 跳转到购物车
         * @constructor
         */
        MyCat() {
            sessionStorage.setItem("username", this.userInfo.username);
            window.location.href = `./GoWuChe.html`;
        },
        /**
         * 得到用户的购物车信息
         * @returns {Promise<void>}
         */
        async getUserCar() {
            console.log("得到用户购物车," + this.userInfo.username);
            if (this.userInfo.username !== "" && this.userInfo.username !== null && this.userInfo.username !== undefined) {
                const {
                    data: res
                } =
                    await this.$http.get(
                        `person/generator/shoppingcar/carListUser/${this.userInfo.username}/${this.status}`);
                console.log("-----------得到用户购物车----------");
                console.log(res.shoppingList);
                this.UserCarList = [];
                res.shoppingList.forEach(async (element) => {
                    // 去查询一个商品的url
                    const {
                        data: res
                    } = await this.$http.get(`goods/generator/petentity/info/${element.goodsNumber}`)
                    if (res.petEntity.status !== 0) {
                        // 如果被下架了不加入
                        this.UserCarList.push({
                            url: res.petEntity.url,
                            goodsName: res.petEntity.petName
                        });
                    }
                });
                console.log('购物车信息');
                console.log(this.UserCarList);
            }
        },
        /**
         * 得到所有的轮播商品信息
         * @returns {Promise<void>}
         */
        async getLunBoList() {
            const {
                data: res
            } = await this.$http.post(`goods/generator/petentity/lunboGoods/${this.LunboNumber}`);
            console.log(res);
            if (res.code === 0) {
                this.LunboList = res.petEntityList;
            } else {
                this.falt = 0;
                this.$message.error('轮播数据获取失败!');
            }
            console.log(this.LunboList);
            console.log(this.falt);
        },
        /**
         * 得到所有的商品信息<分页>
         * @returns {Promise<void>}
         */
        async getGoodsListAll() {
            const {
                data: res
            } = await this.$http.post(`goods/generator/petentity/list`, {
                "page": this.pageIndex,
                "limit": this.pageSize,
                "key": this.key
            });
            console.log("------这是一个页面的20个商品的list----");
            console.log(res);
            // this.goodsTypeList = res.data;
            // console.log(this.goodsTypeList);
            this.goodsListAll = res.page.list;
            console.log(this.goodsListAll);
        },
        /**
         * 添加商品到用户购物车
         * @param goods
         * @returns {Promise<void>}
         */
        async insertShappingCar(goods) {
            let username = this.userInfo.username;
            if (username == '' || username == undefined || username == null) {
                // 弹出去往登录界面的框
                this.$alert('未登录，请进入登录界面进行登录', '提示', {
                    confirmButtonText: '确定',
                    callback: action => {
                        this.$message({
                            type: 'info',
                            message: `action: ${action}`
                        });
                        window.location.href = './login.html';
                    }
                });
                return;
            }
            // 用户名
            // 商品id
            console.log("添加到购物车");
            console.log(username + "," + goods);
            const {
                data: res
            } = await this.$http.post(`person/generator/shoppingcar/save`, {
                username: username,
                goodsNumber: goods,
                num: 1
            });
            console.log(res);
            if (res.code === 0) {
                this.$message({
                    message: '添加到购物车成功!',
                    type: 'success'
                });
            } else {
                this.$message.error('添加到购物车失败!' + res.msg);
            }
            // 刷新
            this.getUserCar();
        },
        /**
         * 添加商品到用户收藏夹
         * @param goods
         * @returns {Promise<void>}
         */
        async insertCollect(goods) {
            let username = this.userInfo.username;
            if (username === '' || username === undefined || username === null) {
                // 弹出去往登录界面的框
                this.$alert('未登录，请进入登录界面进行登录', '提示', {
                    confirmButtonText: '确定',
                    callback: action => {
                        this.$message({
                            type: 'info',
                            message: `action: ${action}`
                        });
                        window.location.href = './login.html';
                    }
                });
                return;
            }
            console.log("添加到收藏夹");
            console.log(username + "," + goods);
            // this.$message.success(username);
            const {
                data: res
            } = await this.$http.post(`/person/generator/usercollect/save`, {
                username: username,
                goodsNumber: goods
            });
            console.log(res);
            if (res.code === 0) {
                this.$message.success('添加到收藏夹成功!');
            } else {
                this.$message.error("添加到收藏夹失败!" + res.msg);
            }
        },
        /**
         * 得到用户推荐商品
         * @returns {Promise<void>}
         */
        async getRecommendUserList() {
            if (this.userInfo.username !== "" && this.userInfo.username != null) {
                // 查找usermapping
                const {
                    data: res1
                } = await this.$http.get(`person/generator/user/info/${this.userInfo.username}`);
                console.log("用户信息!");
                console.log(res1.user);
                const {
                    data: res
                } = await this.$http.get(`recommend/generator/recommond/goods/${res1.user.usernameMapping}`);
                console.log("推荐内容!");
                console.log(res);
                if (res.recommondUser != null) {
                    // 使用goods_mapping 去查询goods相关信息
                    res.recommondUser.recommendations.forEach(async (element) => {
                        const {
                            data: res
                        } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                        if (res.goods.status === 1) {
                            this.RecommendUserGoodsList.push({
                                url: res.goods.url,
                                score: element.score,
                                goodsName: res.goods.petName,
                                beizhu: res.goods.beizhu,
                                goodsNumber: res.goods.goodsNumber
                            });
                        }
                    });
                } else {
                    // 大众推荐
                    const {
                        data: res
                    } = await this.$http.get(`recommend/generator/recommond/over`);
                    // console.log("大众推荐内容!");
                    // console.log(res);
                    // 使用goods_mapping 去查询goods相关信息
                    res.overList.forEach(async (element) => {
                        const {
                            data: res
                        } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                        if (res.goods.status === 1) {
                            this.RecommendUserGoodsList.push({
                                url: res.goods.url,
                                score: element.score,
                                goodsName: res.goods.petName,
                                beizhu: res.goods.beizhu,
                                goodsNumber: res.goods.goodsNumber
                            });
                        }
                    });
                }
                // console.log(this.RecommendUserGoodsList.length);
                console.log('推荐内容----------->');
                console.log(this.RecommendUserGoodsList);
            } else {
                // 大众推荐
                const {
                    data: res
                } = await this.$http.get(`recommend/generator/recommond/over`);
                // console.log("大众推荐内容!");
                console.log(res);
                // 使用goods_mapping 去查询goods相关信息
                res.overList.forEach(async (element) => {
                    const {
                        data: res
                    } = await this.$http.get(`goods/generator/petentity/goods_mapping/${element.goods_mapping}`);
                    this.RecommendUserGoodsList.push({
                        url: res.goods.url,
                        count: element.count,
                        goodsName: res.goods.petName,
                        beizhu: res.goods.beizhu,
                        goodsNumber: res.goods.goodsNumber
                    });
                });
                console.log("大众推荐内容!");
                console.log(this.RecommendUserGoodsList);
            }
        },
        /**
         * 搜索商品，跳转到搜索页面，并传入搜索key
         * @returns {Promise<void>}
         */
        async search() {
            window.location.href = `./list.html`;
            // ?username=${this.userInfo.username}&key=${this.key}
            sessionStorage.setItem('username', this.userInfo.username);
            sessionStorage.setItem('search_key', this.key);
        },
        /**
         * 获取公告栏信息
         * @returns {Promise<void>}
         */
        async getMessage() {
            const {
                data: res
            } = await this.$http.post(`/person/generator/userggao/list`, {
                'page': this.Message.pageIndex,
                'limit': this.Message.pageSize,
            });
            console.log("这是公告栏------>");
            console.log(res);
            this.MessageInfoList = res.page.list;
        },
        /**
         * 获取所有的推荐信息
         * @returns {Promise<void>}
         */
        async getAllRecommend() {
            const {
                data: res
            } = await this.$http.get('recommend/getRecommend');
            if(res.code === 0) {
                console.log("获取成功所有mongodb信息")
                console.log(res);
                res.data.forEach(async (element) => {
                    const {
                        data: res1
                    } = await this.$http.get(`goods/generator/petentity/info/${element.pet_id}`);
                    this.RecommendUserGoodsList.push({
                        name: res1.petEntity.petName,
                        beizhu: res1.petEntity.beizhu,
                        url: res1.petEntity.url,
                        goodsNumber: res1.petEntity.goodsNumber
                    });
                });
            }
            else{
                this.$message.error("获取推荐商品列表失败");
            }
        },
        /**
         * 去重操作
         */
        quchong() {
            var arr = this.recommandGoosList2;
            var newArr = [];
            var newList = [];
            for (var i = 0; i < arr.length; i++) {
                if (newArr.indexOf(arr[i].name) === -1) {
                    newArr.push(arr[i].name);
                    newList.push(arr[i]);
                }
            }
            this.recommandGoosList2 = newList;
            console.log("去重后---->");
            console.log(this.recommandGoosList2);
        },
        /**
         * 跳转到个人中心，并传入相关信息
         * 跳转标志：gr
         * 存入用户名：username
         */
        personZX() {
            //判断是否登录  如果没有登录  不能进行登录操作
            console.log("用户信息====>" + this.userInfo.username);
            if (this.userInfo.username === '' ||
                this.userInfo.username == null) {
                this.$message.error("未登录！请登录后才能进入个人中心!");
                window.location.href = './login.html';
            } else {
                //跳转到个人中心
                sessionStorage.setItem('url_type', 'gr');
                sessionStorage.setItem('username', this.userInfo.username);
                window.location.href = `./person.html`;
            }
        },

        /**
         * 点击宠物进入宠物详情页
         * @param goodsNumber 宠物id
         */
        gotoPet(goodsNumber) {
            sessionStorage.setItem('username', this.userInfo.username);
            sessionStorage.setItem('goodsNumber', goodsNumber);
            window.location.href = `./message.html`;
        },
        /**
         * 搜索宠物
         * @param name
         */
        searchGoods(name) {
            this.key = name;
            this.search();
        }
    },
    mounted() {
    },
    created() {
        // 页面加载完毕
        this.init();
        this.getMenu();
        this.getLunBoList();
        // 得到公告信息
        this.getMessage();

        this.getUserCar();
        this.getGoodsListAll();
        // 得到推荐
        this.getAllRecommend();
        // this.getRecommendUserList();
    },
    // 这里运行js代码
    updated() {
        loading();
    },
    mounted() {
        loading2();
    },

    beforeDestroy() {
    },
})
