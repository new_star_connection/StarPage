$(function () {
  $(".hover").mouseover(function () {
    $(".delobj").removeClass("hide");
  });
  $(".hover").mouseout(function () {
    $(".delobj").addClass("hide");
  });

  $(".tips").mouseover(function () {
    $(".box-sd1").show();
  });
  $(".tips").mouseout(function () {
    $(".box-sd1").hide();
  });


  $(".delobj").click(function () {
    $("#motai-wai").css({
      "visibility": "visible",
    });
    $("#motai").css({
      "visibility": "visible",
    });
  });
  $(".new_add").click(function () {
    $("#motai-wai").css({
      "visibility": "visible",
    });
    $("#motai").css({
      "visibility": "visible",
    });
  });
  $(".closed").click(function () {
    $("#motai").css({
      "visibility": "hidden",
    });
    $("#motai-wai").css({
      "visibility": "hidden",
    });
  });

  $(".payment .btn-check").each(function () {
    var _this = $(this);
    $(this).click(function () {
      $(".payment .btn-check").removeClass("btn-checked");
      _this.addClass("btn-checked");
      $(".payment .c-i").removeClass("chose_icon");
      _this.find(".c-i").addClass("chose_icon");
    });
  });

  $(".default_address").click(function () {
    $(this).find(".c-i").toggleClass("checkbox_chose");
  });

  $(".lb_check").each(function () {
    var _this = $(this);
    _this.click(function () {
      $(".lb_check").find("span").removeClass("radio_chose");
      _this.find("span").addClass("radio_chose");
    });
  });

});


axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios;




var vm = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    userInfo: {
      username: '',
      password: '',
      roleName: '',
    },
    payInfo: {
      outTradeNo: '10002',
      subject: 'xiaomishouji',
      totalAmount: 1,
      body: 'wu'
    },
    order_num: '',
    orderInfo: {
      orderNum: '',
      username: '',
      ordetStatus: 0,
      ordetPayStatus: 0,
      orderMoney: 0,
      goodsSum: 0,
      starTime: "",
      updateTime: "",
      endaddress: "",
      telnumber: "",
      beizhu: "",
    }
  },
  methods: {
    async init() {
      var url = window.location.href; //获取当前url
      var dz_url = url.split('#')[0]; //获取#/之前的字符串
      var cs = dz_url.split('?')[1]; //获取?之后的参数字符串
      var cs_arr = cs.split('&'); //参数字符串分割为数组
      var cs = {};
      console.log(cs_arr);
      for (var i = 0; i < cs_arr.length; i++) { //遍历数组，拿到json对象
        cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
      }
      this.userInfo.username = cs.username; //这样就拿到了参数中的数据
      this.order_num = cs.order_num
      console.log(this.userInfo.username);
      console.log(this.order_num);
      // 去拿到改订单的详细信息
      const {
        data: res
      } = await this.$http.get(`/order/generator/ordermaster/info/${this.order_num}`);
      console.log("详细信息!dd");
      console.log(res);
      this.orderInfo = res.orderMaster;
      console.log(this.orderInfo);
      if (res.code !== 0) {
        this.$message.error('未获取到详细信息！可能已经被修改');
        return;
      }

    },
    strToJson(str) {
      var json = eval('(' + str + ')');
      return json;
    },
    Submit() {
      console.log('submit!')
      // 重写订单详情
      this.payInfo.outTradeNo = this.order_num;
      this.payInfo.subject = this.userInfo.username;
      this.payInfo.totalAmount = this.orderInfo.orderMoney;
      this.payInfo.body = this.orderInfo.beizhu;
      this.$http.post('/third/order/alipay', this.payInfo).then(resp => {
        // 添加之前先删除一下，如果单页面，页面不刷新，添加进去的内容会一直保留在页面中，二次调用form表单会出错
        console.log("返回的数据：----->");
        console.log(resp);
        console.log("-------------------------");
        let data = {};
        data = this.strToJson(resp.config.data);
        window.location.href = `./index.html?username=${data.subject}`;
        const divForm = document.getElementsByTagName('div')
        if (divForm.length) {
          document.body.removeChild(divForm[0])
        }
        const div = document.createElement('div')
        div.innerHTML = resp.data // data就是接口返回的form 表单字符串
        document.body.appendChild(div)
        document.forms[0].setAttribute('target', '_blank') // 新开窗口跳转
        document.forms[0].submit();
      });

    }
  },
  created() {
    this.init();
  },
})