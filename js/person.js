$(function () {
    $('.right-li2').mouseover(function () {
        $('.right-dropdown').show()
        $('.right-div1').css({
            'background-color': '#FFFFFF',
            border: '1px solid #E6E6E6',
            'border-bottom': 'none',
        })
        $('.right-div1 a').css({
            color: '#E3101E',
        })
    })
    $('.right-li2').mouseout(function () {
        $('.right-dropdown').hide()
        $('.right-div1').css({
            'background-color': 'transparent',
            border: '1px solid transparent',
            'border-bottom': 'none',
        })
        $('.right-div1 a').css({
            color: '#5E5E5E',
        })
    })
    //	会员俱乐部
    $('.g').each(function () {
        $(this).mouseover(function () {
            $(this).find('.navList').show()
            $(this).children('a').css({
                'background-color': '#FFFFFF',
                border: '1px solid #E6E6E6',
                'border-bottom': 'none',
            })
            $(this).children('a').css({
                color: '#E3101E',
            })
            $(this).children('a').children('i').css({
                'background-position': '-12px 0',
            })
        })
        $(this).mouseout(function () {
            $(this).find('.navList').hide()
            $(this).children('a').css({
                'background-color': 'transparent',
                border: '1px solid transparent',
                'border-bottom': 'none',
            })
            $(this).children('a').css({
                color: '#FFFFFF',
            })
            $(this).children('a').children('i').css({
                'background-position': '0 0',
            })
        })
    })
    //	会员俱乐部

    //	头像
    $('.cont-top-headphoto').mouseover(function () {
        $('.edit').stop().animate({
                opacity: '1',
            },
            500
        )
    })
    $('.cont-top-headphoto').mouseout(function () {
        $('.edit').stop().animate({
                opacity: '0',
            },
            500
        )
    })
    //	头像

    //showMoreBtn
    $('.arow').click(function () {
        $(this).toggleClass('showMoreBtn')
        $(this).toggleClass('hideBtn')
        $('.hideBtn').html('收起')
        $('.showMoreBtn').html('更多类型')
        $('.dn').toggle()
    })

    //showMoreBtn

    //订单下拉列表
    $('.dindan').each(function () {
        var _this = $(this)
        $(this).click(function () {
            _this.siblings('.c-list-value').toggle()
        })
    })
    //订单下拉列表

    //猜你喜欢轮播图
    var bannerPage = 1
    var bannerLeft
    $('.bigData_btn_left').click(function () {
        bannerPage--
        if (bannerPage == 0) {
            bannerLeft = -1900
            bannerPage = 3
        } else {
            bannerLeft = -(bannerPage - 1) * 950
        }

        $('.order-like-content ul')
            .stop()
            .animate({
                    left: bannerLeft + 'px',
                },
                500
            )
    })

    $('.bigData_btn_right').click(function () {
        bannerPage++
        if (bannerPage == 4) {
            bannerLeft = 0
            bannerPage = 1
        } else {
            bannerLeft = -(bannerPage - 1) * 950
        }

        $('.order-like-content ul')
            .stop()
            .animate({
                    left: bannerLeft + 'px',
                },
                500
            )
    })

    //猜你喜欢轮播图

    $('.l-slider li').mouseover(function () {
        $('.l-slider li').removeClass('active')
        $(this).addClass('active')
    })
    $('.l-slider li').mouseout(function () {
        $('.l-slider li').removeClass('active')
        $('.current').addClass('active')
    })

    $('.attrV ul li a').each(function () {
        var _this = $(this)
        $(this).click(function () {
            $('.attrV ul li a').removeClass('cur')
            _this.addClass('cur')
        })
    })
})

function getUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        return (c === 'x' ? (Math.random() * 16) | 0 : 'r&0x3' | '0x8').toString(16)
    })
}

axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios

var vm = new Vue({
    el: '#appPerson',
    data: {
        url_type: 'gr',
        userInfo: {
            username: '',
            password: '',
            roleName: '用户-发布者',
            email: '',
            userTelephone: '',
        },
        goodsTypeList: [],
        goods: {},
        goodstype: [],
        type: '',
        orderInfoList: [],
        OrderDetialdialogVisible: false,
        orderDetailList: [],
        RecommendUserGoodsList: [],
        orderNum: '',
        orderDetialInfoList: [],
        usercollect: {
            url: '',
            goodsName: '',
        },
        usercollectList: [],
        dataForm: {
            goodsNumber: '',
            typeNumber: 0,
            petName: '',
            beizhu: '',
            petAge: 0,
            status: 1,
            sort: 0,
            url: '',
            address: '',
            mianyi: 0,
            xxaddress: '',
            userTelephone: '',
        },
        dataUserRule: {
            username: [{
                required: true,
                message: '账号不能为空',
                trigger: 'blur',
            },],
            password: [{
                required: true,
                message: '密码不能为空',
                trigger: 'blur',
            },],
            email: [{
                required: true,
                message: '邮箱不能为空',
                trigger: 'blur',
            },],
            userTelephone: [{
                required: true,
                message: '手机号码不能为空',
                trigger: 'blur',
            },
                {
                    min: 11,
                    max: 11,
                    message: '手机号码长度必须为11位',
                },
            ],
        },

        dataRule: {
            petName: [{
                required: true,
                message: '宠物名不能为空',
                trigger: 'blur',
            },],
            url: [{
                required: true,
                message: '宠物图片url地址不能为空',
                trigger: 'blur',
            },],
            beizhu: [{
                required: true,
                message: '备注不能为空',
                trigger: 'blur',
            },],
            address: [{
                required: true,
                message: '地区不能为空',
                trigger: 'blur',
            },],
            showStatus: [{
                required: true,
                message: '显示状态[0-不显示；1-显示]不能为空',
                trigger: 'blur',
            },],
            sort: [{
                validator: (rule, value, callback) => {
                    if (value == '') {
                        callback(new Error('排序字段必须填写'))
                    } else if (!Number.isInteger(value) || value < 0) {
                        callback(new Error('排序必须是一个大于等于0的整数'))
                    } else {
                        callback()
                    }
                },
                trigger: 'blur',
            },],
        },
        //图片上传鉴权
        dataObj: {
            policy: '',
            signature: '',
            key: '',
            ossaccessKeyId: '',
            dir: '',
            host: '',
        },
        //发布显示
        dialogVisible: false,
        // 图片list
        fileList: [],
        showfilelist: false,
        dialog: false,
        loading: false,
        // 获取的宠物list
        userPetList: [],
        editForm: {
            goodsNumber: '',
            typeNumber: 0,
            petName: '',
            beizhu: '',
            status: 1,
            sort: 0,
            url: '',
        },
        //编译框显示
        visible: false,
        editvisible: false,
        status: 0
    },
    methods: {
        async init() {
            //之前使用url拼窜，现在使用session
            // var url = window.location.href; //获取当前url
            // var dz_url = url.split('#')[0]; //获取#/之前的字符串
            // var cs = dz_url.split('?')[1]; //获取?之后的参数字符串
            // console.log(cs)
            // if (cs == undefined) return;
            // var cs_arr = cs.split('&'); //参数字符串分割为数组
            // var cs = {};
            // console.log(cs_arr);
            // for (var i = 0; i < cs_arr.length; i++) { //遍历数组，拿到json对象
            //     cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
            // }
            this.state = sessionStorage['status'];
            this.url_type = sessionStorage['url_type'];
            this.userInfo.username = sessionStorage['username']; //这样就拿到了参数中的数据
            console.log(this.userInfo.username)
            const {data: res} = await this.$http.get(
                `/person/generator/user/info/${this.userInfo.username}`
            )
            console.log("=====================");
            console.log(res);
            this.userInfo.password = res.user.password;
            this.userInfo.email = res.user.email;
            this.userInfo.userTelephone = res.user.userTelephone;

            if (this.state === 0) {
                // 去拿到这个人的身份
                const {data: res} = await this.$http.get(
                    `/person/generator/userrole/user_role_info/${this.userInfo.username}`
                )
                res.userRoleList.forEach(async (element) => {
                    // 去查roleName
                    const {data: res1} = await this.$http.get(
                        `person/generator/role/info/${element.roleNumber}`
                    )
                    if (res1.role.roleName === '发布者') {
                        this.userInfo.roleName = '发布者'
                    }
                })
            } else {
                this.userInfo.roleName = "买家";
            }
        },
        /**
         * 获得用户收藏夹信息
         */
        async getCollect() {
            const {data: res1} = await this.$http.get(
                `person/generator/usercollect/info/${this.userInfo.username}`
            )
            console.log(res1)
            this.usercollectList = []
            this.usercollect = res1.userCollect
            //	console.log(this.usercollect);
            res1.userCollect.forEach(async (element) => {
                const {data: res} = await this.$http.get(
                    `goods/generator/petentity/info/${element.goodsNumber}`
                )
                console.log(res)
                if (res.petEntity.status == 1) {
                    const {data: res2} = await this.$http.get(
                        `goods/generator/pettype/info/${res.petEntity.typeNumber}`
                    )
                    console.log(res2)
                    this.usercollectList.push({
                        petName: res.petEntity.petName,
                        typeName: res2.petType.typeName,
                        person: res.petEntity.person,
                        address: res.petEntity.address,
                        beizhu: res.petEntity.beizhu,
                        url: res.petEntity.url,
                        goodsNumber: res.petEntity.goodsNumber,
                    })
                }
            })
            console.log('这是用户的收藏夹')
            console.log(this.usercollectList)
        },
        /**
         * 获取购物车
         * @constructor
         */
        MyCat() {
            window.location.href = `./GoWuChe.html?username=${this.userInfo.username}`
        },
        /**
         * 返回首页
         * @constructor
         */
        ReturnFirstPage() {
            window.location.href = `./index.html?username=${this.userInfo.username}`
        },
        /**
         * 跳转标签
         * @param item
         * @constructor
         */
        JupmPage(item) {
            this.url_type = item
            console.log('============>' + this.url_type)
            // window.location.reload();
            // window.location.href = `./` + item + `.html`;
        },
        /**
         * 得到用户推荐
         */
        async getRecommendUserList() {
            const {
                data: res
            } = await this.$http.get('recommend/getRecommend');
            if (res.code === 0) {
                console.log("获取成功所有mongodb信息")
                console.log(res);
                res.data.forEach(async (element) => {
                    const {
                        data: res1
                    } = await this.$http.get(`goods/generator/petentity/info/${element.pet_id}`);
                    this.RecommendUserGoodsList.push({
                        name: res1.petEntity.petName,
                        beizhu: res1.petEntity.beizhu,
                        url: res1.petEntity.url,
                        goodsNumber: res1.petEntity.goodsNumber
                    });
                });
            } else {
                this.$message.error("获取推荐商品列表失败");
            }
        },
        /**
         * 跳转到推荐的商品页
         */
        gotoPet(goodsNumber) {
            sessionStorage.setItem('username', this.userInfo.username)
            sessionStorage.setItem('goodsNumber', goodsNumber)
            window.location.href = `./message.html`
        },
        async deleteByGoosNumber(goodsNumber) {
            console.log(goodsNumber)
            this.$confirm(`确定对[goodsNumber=${goodsNumber}]进行删除操作?`, '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
            }).then(async () => {
                // 发送请求
                const {data: res1} = await this.$http.get(
                    `/person/generator/usercollect/delete/${this.userInfo.username}/${goodsNumber}`
                )
                console.log('删除收藏夹')
                console.log(res1)
                if (res1.code == 0) {
                    this.$message({
                        message: '操作成功',
                        type: 'success',
                        duration: 1500,
                        onClose: () => {
                            this.getCollect()
                        },
                    })
                } else {
                    this.$message.error('删除失败!' + res1.msg)
                }
            })
        },
        /**
         * 拿到上传图片到阿里云oss的许可证
         * @returns {Promise<unknown>}
         */
        policy() {
            return new Promise(async (resolve, reject) => {
                console.log('到了')
                const {data: data} = await this.$http.get('/third/oss/policy')
                console.log(data)
                resolve(data)
            })
        },
        handleRemove(file, fileList) {
            console.log(file, fileList)
        },
        handlePreview(file) {
            console.log('handlePreview')
            console.log(file)
            this.dialogVisible = true
        },
        beforeUpload(file) {
            const _self = this
            return new Promise((resolve, reject) => {
                console.log('得到了，上传前')
                this.policy()
                    .then((response) => {
                        _self.dataObj.policy = response.data.policy
                        _self.dataObj.signature = response.data.signature
                        _self.dataObj.ossaccessKeyId = response.data.accessid
                        _self.dataObj.key = response.data.dir + getUUID() + '_${filename}'
                        _self.dataObj.dir = response.data.dir
                        _self.dataObj.host = response.data.host
                        console.log(_self.dataObj)
                        console.log('成功~')
                        resolve(true)
                    })
                    .catch((err) => {
                        reject(false)
                    })
            })
        },
        successUpload(response, file) {
            // this.fileList.pop()
            this.showfilelist = true
            console.log('successUpload')
            // console.log(this.fileList);
            // this.fileList.push({
            //   name: file.name,
            //   url: this.dataObj.host + '/' + this.dataObj.key.replace('${filename}', file.name)
            // })
            // console.log(this.fileList);
            console.log(
                this.dataObj.host +
                '/' +
                this.dataObj.key.replace('${filename}', file.name)
            )
            this.dataForm.url =
                this.dataObj.host +
                '/' +
                this.dataObj.key.replace('${filename}', file.name)
            this.editForm.url = this.dataForm.url
            // console.log(file);
        },
        // 提交
        onSubmit() {
            console.log('submit!')
        },
        //提交发布表单
        async dataFormSubmit() {
            console.log('----开始提交----')
            console.log(this.dataForm)
            this.dialog = false
            const {data: res} = await this.$http.post(
                `goods/generator/petentity/save`, {
                    petName: this.dataForm.petName,
                    petAge: this.dataForm.petAge,
                    mianyi: this.mianyi,
                    person: this.userInfo.username,
                    address: this.dataForm.address,
                    xxaddress: this.dataForm.xxaddress,
                    userTelephone: this.dataForm.userTelephone,
                    checkFlat: 0,
                    ly: 0,
                    status: 1,
                    beizhu: this.dataForm.beizhu,
                    url: this.dataForm.url,
                }
            )
            if (res.code == 0) {
                console.log(res)
                this.$message.success('发布成功!')
            } else {
                this.$message.error('发布失败,' + res.msg)
            }
        },
        //提交表单的加载动画
        handleClose() {
            if (this.loading) {
                return
            }
            this.$confirm('确定要提交表单吗？')
                .then((_) => {
                    this.loading = true
                    this.timer = setTimeout(() => {
                        // 动画关闭需要一定的时间
                        setTimeout(() => {
                            this.loading = false
                            this.dialog = false
                            // done();
                            // 完成提交
                            this.dataFormSubmit()
                        }, 400)
                    }, 2000)
                    // 刷新
                    // this.getuserPetList();
                })
                .catch((_) => {
                })
        },
        //取消提交
        cancelForm() {
            this.loading = false
            this.dialog = false
            clearTimeout(this.timer)
        },
        //从后台获取到 发布者 已经发布后的流浪宠物
        async getuserPetList() {
            const {data: res} = await this.$http.post(
                `goods/generator/petentity/username/${this.userInfo.username}`
            )
            // this.userPetList = res.goodsList;
            console.log(res)
            this.userPetList = []
            res.goodsList.forEach((element) => {
                if (element.status !== 0) {
                    this.userPetList.push(element)
                }
            })
            if (res.code == 0) {
                this.$message.success('成功获取信息!')
            }
            console.log('获取到用户发布信息')
            console.log(this.userPetList)
        },
        //获取需要更新的 宠物信息
        async addOrUpdateHandle(goodsNumber) {
            // 编译框
            this.visible = true
            const {data: res} = await this.$http.get(
                `/goods/generator/petentity/info/${goodsNumber}`
            )
            console.log('更新~~~')
            console.log(res)
            this.editForm = res.petEntity
            this.showfilelist = true
            console.log(this.editForm)
        },
        //修改提交
        async editSubmit() {
            const {data: res} = await this.$http.post(
                `/goods/generator/petentity/update`, {
                    petName: this.editForm.petName,
                    address: this.editForm.address,
                    goodsNumber: this.editForm.goodsNumber,
                    url: this.editForm.url,
                    beizhu: this.editForm.beizhu,
                }
            )
            this.visible = false
            console.log('确认更新')
            console.log(res)
            if (res.code == 0) {
                this.$message.success('更新成功~')
            } else {
                this.$message.error('更新失败~,' + res.msg)
            }
            // 刷新
            this.getuserPetList()
        },
        //删除发布的宠物
        async deleteHandle(goodsNumber) {
            this.$confirm(`确定对[id=${goodsNumber}]进行删除操作?`, '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
            }).then(async () => {
                // console.log("daole")
                const {data: res} = await this.$http.post(
                    `/goods/generator/petentity/delete/${goodsNumber}`
                )
                console.log('确认删除')
                console.log(res)
                if (res.code == 0) {
                    this.$message.success('删除成功!')
                } else {
                    this.$message.error('删除失败!' + res.msg)
                }
                this.getuserPetList()
            })
        },
        //打开修改框
        edit() {
            this.editvisible = true
        },
        /**
         * 获得订单主表信息
         * @returns {Promise<void>}
         */
        async getOrderList() {
            const {
                data: res
            } = await this.$http.get(`/order/generator/ordermaster/infoByUsername/${this.userInfo.username}`);
            console.log('用户订单!');
            console.log(res);
            this.orderInfoList = res.orderMasterList;
        },
        /**
         * 获得订单详情表信息
         * @param orderNum
         * @returns {Promise<void>}
         */
        async detialOrder(orderNum) {
            // 查询
            this.orderNum = orderNum;
            const {
                data: res
            } = await this.$http.get(`/order/generator/orderdetail/infoByMasterNum/${orderNum}`);
            console.log(res);
            this.orderDetialInfoList = [];
            // 查询goodsName
            // this.orderDetialInfo = res.orderDetailList;
            res.orderDetailList.forEach(async (element) => {
                const {
                    data: res
                } = await this.$http.get(`/goods/generator/petentity/info/${element.goodsNumber}`);
                this.orderDetialInfoList.push({
                    goodsName: res.petEntity.petName,
                    orderDtId: element.orderDtId,
                    orderNum: element.orderNum,
                    goodsPrice: element.goodsPrice,
                    goodsNums: element.goodsNums,
                    goodsUrl: element.goodsUrl,
                    goodsNumber: element.goodsNumber
                });
            });
            this.OrderDetialdialogVisible = true;
        },
        /**
         * 提交用户修改信息
         * @returns {Promise<void>}
         */
        async editUserSubmit() {
            const {data: res} = await this.$http.post("/person/generator/user/update", this.userInfo);
            if(res.code===0){
                this.$message.success('修改成功');
            }
            else{
                this.$message.error('修改失败');
            }
            this.editvisible=false;
        }

    },
    created() {
        this.init()
        //获取用户收藏夹信息
        this.getCollect();
        //获取用户订单信息
        this.getOrderList();

        this.getRecommendUserList();
    },
})
