$(function () {
    //	处理地址
    $("#citySelect a").each(function () {
        var _this = $(this);
        var index = $(this).index();
        $(this).click(function () {
            $("#citySelect a").removeClass("cur");
            _this.addClass("cur");
            $(".gctBox").hide();
            $(".gctBox").eq(index).show();
        });
    });

    var address = "";
    var address2 = "";

    $(".sheng span").each(function () {
        var _this = $(this);
        var value = $(this).find("a").html();

        //		alert(value);
        $(this).click(function () {
            address = address + value;
            address2 = address2 + value;
            $(".shen-a").removeClass("cur");
            $(".shi-a").addClass("cur");
            $(".shen-a b").html(value);
            $(".sheng").hide();
            if (value == "福建省") {
                $(".xiamen").show();
            }
        });

    });

    $(".shi span").each(function () {
        var _this = $(this);
        var value = $(this).find("a").html();

        //		alert(value);
        $(this).click(function () {
            address = address + value;
            address2 = address2 + value;
            $(".shi-a").removeClass("cur");
            $(".area-a").addClass("cur");
            $(".shi-a b").html(value);
            $(".shi").hide();
            if (value == "厦门市") {
                $(".simingqu").show();
            }
        });

    });

    $(".area span").each(function () {
        var _this = $(this);
        var value = $(this).find("a").html();

        //		alert(value);
        $(this).click(function () {
            address = address + value;
            address2 = address2 + value;
            $(".area-a").removeClass("cur");
            $(".jiedao-a").addClass("cur");
            $(".area-a b").html(value);
            $(".area").hide();
            if (value == "思明区") {
                $(".binhai").show();
            }
        });

    });

    $(".jiedao span").each(function () {
        var _this = $(this);
        var value = $(this).find("a").html();

        $(this).click(function () {
            address = address + value;
            $(".jiedao-a").removeClass("cur");
            $(".jiedao-a").addClass("cur");
            $(".jiedao-a b").html(value);
            $(".gCity").hide();
            $("#stockaddress").html(address);
            address = address2;
            vm.endAddress = address;
            // 再执行一次列表钱刷新
            vm.getUserCar();
        });

    });

    $("#address").click(function () {
        $(".gCity").show();
    });

    $("#cityClose").click(function () {
        $(".gCity").hide();
    });

    //	处理地址

    //结算
    var s = $(".goods-price").html();
    var num = s.replace(/[^0-9]/ig, "");
    num = parseInt(num);
    num = num / 100;
    num = num.toFixed(2);

    //  alert(num);//169.00

    // $(".gui-count-add").click(function () {
    //   var v = $("#num").attr("value");
    //   var maxlength = $("#num").attr("maxlength");
    //   if (v < maxlength) {
    //     v++;
    //     $("#num").attr("value", v);
    //   }
    //   var total = v * num;
    //   total = total.toFixed(2);
    //   var red_total = total;
    //   red_total = "￥" + red_total;
    //   total = "¥&nbsp;" + total;
    //   $(".total").html(red_total);
    //   $(".goods-price").html(total);
    //   $(".goods-num").html($("#num").attr("value"));
    // });

    // $(".gui-count-sub").click(function () {
    //   var v = $("#num").attr("value");
    //   var minlength = 1;
    //   if (v > minlength) {
    //     v--;
    //     $("#num").attr("value", v);
    //   }
    //   var total = v * num;
    //   total = total.toFixed(2);
    //   var red_total = total;
    //   red_total = "￥" + red_total;
    //   total = "¥&nbsp;" + total;
    //   $(".total").html(red_total);
    //   $(".goods-price").html(total);
    //   $(".goods-num").html($("#num").attr("value"));
    // });

    //结算

    $(".ci").each(function () {
        $(this).click(function () {
            $(".ci").toggleClass("checkboxs");
        });
    });

});

// document.write("<script src='./vue@2.6.11.js'></script>");
// document.write("<script src='./axios.min.js'></script>");

axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios;

// import {
//     regionData
// } from "element-china-area-data";

var vm = new Vue({
    el: '#app',
    data: {
        selectAddress: false,
        areaList: rawCitiesData,
        optionProps: { //配置节点
            value: 'name',
            label: 'name',
            children: 'sub'
        },
        message: 'Hello Vue!',
        userInfo: {
            username: '',
            password: '',
            roleName: '',
            telnumber: '',
        },
        goodsTypeList: [],
        UserCarList: [],
        endAddress: '重庆市南岸区',
        selectEndAddress: ['重庆市南岸区'],
        sum_price: 0.0,
        selectAll: false,
        isIndeterminate: false,
        submit_uuid: '',
        select_num: 0,
        status: 0,
    },
    methods: {
        /**
         * 选择位置
         */
        selectEndAddress_handleChangeOpen() {
            this.selectAddress = !this.selectAddress;

        },
        getTextByJs(arr) {
            let str = "";
            for (let i = 0; i < arr.length; i++) {
                str += arr[i] + "";
            }
            //去掉最后一个逗号(如果不需要去掉，就不用写)
            if (str.length > 0) {
                str = str.substr(0, str.length - 1);
            }
            return str;
        },
        selectEndAddress_handleChangeClose() {
            this.selectAddress = !this.selectAddress;
            //发起重新计算价格
            this.endAddress = this.getTextByJs(this.selectEndAddress);
            console.log("----------选择的地址-------------");
            console.log(this.endAddress);
            console.log("********************************");
            this.getUserCar().then(r => {
                console.log("完成重新计算价格");
            });
        },
        async init() {
            // var url = window.location.href; //获取当前url
            // var dz_url = url.split('#')[0]; //获取#/之前的字符串
            // var cs = dz_url.split('?')[1]; //获取?之后的参数字符串
            // var cs_arr = cs.split('&'); //参数字符串分割为数组
            // var cs = {};
            // console.log(cs_arr);
            // for (var i = 0; i < cs_arr.length; i++) { //遍历数组，拿到json对象
            //     cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
            // }
            // 从session中获取数据
            this.userInfo.username = sessionStorage['username']; //这样就拿到了参数中的数据
            this.status = sessionStorage['status'];
            console.log(this.userInfo.username);
            console.log(this.status);
            // 查询 一个电话
            const {
                data: res
            } = await this.$http.get(`/person/generator/user/info/${this.userInfo.username}`);
            this.userInfo.telnumber = res.user.userTelephone;
        },
        MyOrder() {
            this.SubmitOrder();
        },
        async getUserCar() {
            const {
                data: res2
            } =
                await this.$http.get(
                    `person/generator/shoppingcar/carListUser/${this.userInfo.username}/${this.status}`);
            this.UserCarList = [];
            if (this.status === 0) {
                res2.shoppingList.forEach(async (element) => {
                    // 去查询一个商品的url
                    const {
                        data: res
                    } = await this.$http.get(`goods/generator/petentity/info/${element.goodsNumber}`);
                    const {
                        data: res1
                    } = await this.$http.get(`goods/generator/pettype/info/${res.petEntity.typeNumber}`);
                    // const {
                    //   data: res3
                    // } = await this.$http.get(``);
                    if (res.petEntity.address == null) {
                        res.petEntity.address = this.endAddress;
                    }
                    const {
                        data: dinstance
                    } = await this.$http.post('/third/getdistance', {
                        "star": res.petEntity.address,
                        "end": this.endAddress
                    });
                    // console.log("求距离");
                    // console.log(dinstance);
                    if (res.petEntity.status !== 0) {
                        this.UserCarList.push({
                            url: res.petEntity.url,
                            goodsNumber: res.petEntity.goodsNumber,
                            goodsName: res.petEntity.petName,
                            person: res.petEntity.person,
                            address: res.petEntity.address,
                            userTelephone: res.petEntity.userTelephone,
                            address: res.petEntity.address,
                            xxaddress: res.petEntity.xxaddress,
                            detial: res.petEntity.beizhu,
                            petType: res1.petType == null ? "空" : res1.petType.typeName,
                            num: element.num,
                            postMoney: (dinstance.distance * 0.0001 < 1 ? 1 : dinstance.distance * 0.0001).toFixed(3),
                            select: false
                        });
                    }
                });
            } else {
                //购物商场
                res2.shoppingList.forEach(async (element) => {
                    // 去查询一个商品的url
                    const {
                        data: res
                    } = await this.$http.get(`goods/goods_entity/info/${element.goodsNumber}`);
                    // const {
                    //   data: res3
                    // } = await this.$http.get(``);
                    // if (res.goodsEntity.address == null) {
                    //     res.goodsEntity.address = this.endAddress;
                    // }
                    if (res.goodsEntity.status !== 0) {
                        this.UserCarList.push({
                            url: res.goodsEntity.goodsUrl,
                            goodsNumber: res.goodsEntity.goodsId,
                            goodsName: res.goodsEntity.goodsName,
                            goodsPrice: res.goodsEntity.goodsPrice,
                            goodsCount: element.num,
                            select: false
                        });
                    }
                });
            }
            // this.UserCarList = res.shoppingList;
            console.log('购物车信息');
            console.log(this.UserCarList);

        },
        caclulateSum() {
            this.sum_price = 0.0;
            this.select_num = 0.0;
            if (this.status === 0) {
                this.UserCarList.forEach(element => {
                    if (element.select) {
                        this.sum_price += (element.num * element.postMoney);
                        this.select_num += 1;
                    }
                });
                this.sum_price = (this.sum_price).toFixed(3);
                console.log(this.sum_price);
            } else {
                this.UserCarList.forEach((element) => {
                    if (element.select) {
                        this.sum_price += (element.goodsCount * element.goodsPrice);
                        this.select_num += 1;
                    }
                });
            }
        },
        handleCheckAllChange() {
            this.UserCarList.forEach(element => {
                element.select = !element.select;
            });
            this.caclulateSum();
        },
        uuid() {
            var s = [];
            var hexDigits = "0123456789abcdef";
            for (var i = 0; i < 36; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
            s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
            s[8] = s[13] = s[18] = s[23] = "-";

            var uuid = s.join("");
            return uuid;
        },
        async SubmitOrder() {
            Date.prototype.Format = function (fmt) { // author: meizz
                var o = {
                    "M+": this.getMonth() + 1, // 月份
                    "d+": this.getDate(), // 日
                    "h+": this.getHours(), // 小时
                    "m+": this.getMinutes(), // 分
                    "s+": this.getSeconds(), // 秒
                    "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
                    "S": this.getMilliseconds() // 毫秒
                };
                if (/(y+)/.test(fmt))
                    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
                for (var k in o)
                    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
                return fmt;
            }
            console.log("----------开始提交订单------------");
            var myDate = new Date().Format("yyyy-MM-dd hh:mm:ss");
            console.log("mydate:", myDate);
            var mytime = myDate; //获取当前时间
            var uuid = this.uuid();
            this.submit_uuid = uuid;
            console.log("uuid=", uuid);
            goodsList = [];
            selectGoodsNumber = [];
            if(this.status === 0) {
                this.UserCarList.forEach(element => {
                    if (element.select) {
                        goodsList.push({
                            orderNum: uuid,
                            goodsPrice: element.postMoney * 1.0,
                            goodsNums: element.num,
                            goodsUrl: element.url,
                            goodsNumber: element.goodsNumber
                        });
                        selectGoodsNumber.push(element.goodsNumber);
                    }
                });
                this.select_num = goodsList.length;
                let status = this.status;
                const {
                    data: res
                } = await this.$http.post('/order/generator/ordermaster/submit', {
                    "master": {
                        orderNum: uuid,
                        username: this.userInfo.username,
                        ordetStatus: "0",
                        ordetPayStatus: "0",
                        orderMoney: this.sum_price * 1.0,
                        starTime: myDate.toLocaleString(),
                        updateTime: myDate.toLocaleString(),
                        goodsSum: goodsList.length,
                        endAddress: this.endAddress,
                        telnumber: this.userInfo.telnumber,
                        type_status: parseInt(status)
                    },
                    "detialList": goodsList,
                    "selectList": selectGoodsNumber
                });
                console.log("提交结果-------------");
                console.log(res);
                if (res.code == 0) {
                    this.$message.success('提交成功!正在跳转...');
                    // 等待3秒
                    window.location.href = `./DinDan.html?username=${this.userInfo.username}&&order_num=${this.submit_uuid}`;
                } else {
                    this.$message.error('提交失败!' + res.msg);
                }
            }
            else{
                this.UserCarList.forEach(element => {
                    if (element.select) {
                        goodsList.push({
                            orderNum: uuid,
                            goodsPrice: parseFloat(element.goodsPrice * 1.0),
                            goodsNums: element.goodsCount,
                            goodsUrl: element.url,
                            goodsNumber: element.goodsNumber
                        });
                        selectGoodsNumber.push(element.goodsNumber);
                    }
                });
                this.select_num = goodsList.length;
                let status = this.status;
                const {
                    data: res
                } = await this.$http.post('/order/generator/ordermaster/submit', {
                    "master": {
                        orderNum: uuid,
                        username: this.userInfo.username,
                        ordetStatus: "0",
                        ordetPayStatus: "0",
                        orderMoney: this.sum_price * 1.0,
                        starTime: myDate.toLocaleString(),
                        updateTime: myDate.toLocaleString(),
                        goodsSum: goodsList.length,
                        endAddress: this.endAddress,
                        telnumber: this.userInfo.telnumber,
                        type_status: parseInt(status)
                    },
                    "detialList": goodsList,
                    "selectList": selectGoodsNumber
                });
                console.log("提交结果-------------");
                console.log(res);
                if (res.code == 0) {
                    this.$message.success('提交成功!正在跳转...');
                    // 等待3秒
                    window.location.href = `./DinDan.html?username=${this.userInfo.username}&&order_num=${this.submit_uuid}`;
                } else {
                    this.$message.error('提交失败!' + res.msg);
                }
            }
        },
        messageADDandDEC() {
            this.$alert('此宠物只存在一只，无法添加', '提示', {
                confirmButtonText: '确定',
                callback: action => {
                    // this.$message({
                    //   type: 'info',
                    //   message: `action: ${ action }`
                    // });
                }
            });
        },
        gotoFirst() {
            sessionStorage.setItem('username',this.userInfo.username);
            if(this.status===0) {
                window.location.href = `./index.html`;
            }
            else{
                window.location.href = './s_index.html';
            }
        },
        async deleteGoods(item) {
            const {data: res} = await this.$http.post(`/person/generator/shoppingcar/delete_goodsNumber/${item.goodsNumber}/${this.userInfo.username}`);
            if (res.code === 0) {
                this.$message.success('删除成功');
            } else {
                this.$message.error('删除失败');
            }
            this.getUserCar();
        }
    },
    created() {
        this.init();
        this.getUserCar();
    },
    watch: {
        UserCarList() {
            this.caclulateSum();
        }
    },


})
