axios.defaults.baseURL = 'http://localhost:88/'

Vue.prototype.$http = axios



var main = {
  data() {
    var validatePass = (rule, value, callback) => {
      console.log("value=" + this.Twopassword);
      if (this.Twopassword === '') {
        callback(new Error('请输入密码'));
      } else if (this.Twopassword !== this.userInfo.password) {
        callback(new Error('两次输入密码不相同'));
      } else {
        callback();
      }
    };
    var phoneNumber = (rule, value, callback) => {
      if (this.userInfo.userTelephone === '') {
        callback(new Error('请输入电话号码'));
      } else if (!(/^1[3456789]\d{9}$/.test(this.userInfo.userTelephone))) {
        callback(new Error('请输入正确的电话号码'));
      } else {
        callback();
      }
    };
    return {
      message: 'Hello Vue!',
      userInfo: {
        username: '',
        password: '',
        email: '',
        roleName: '',
        userTelephone: '',
        code: '',
      },
      userInfo_rules: {
        username: [{
            required: true,
            message: '用户名不能为空',
            trigger: 'blur'
          },
          {
            min: 6,
            max: 20,
            message: '长度在 6 到 20 个字符',
            trigger: 'blur'
          }
        ],
        password: [{
          required: true,
          message: '密码不能为空',
          trigger: 'blur'
        }, {
          min: 6,
          max: 20,
          message: '长度在6到20个字符',
          trigger: 'blur'
        }],
        userTelephone: [{
            validator: phoneNumber,
            trigger: 'blur'
          },
          {
            min: 11,
            max: 11,
            message: '长度必须为11位',
            trigger: 'blur'
          }
        ],
        code: [{
          required: true,
          message: '验证码不能为空',
          trigger: 'blur'
        }, {
          min: 4,
          max: 4,
          message: '验证码为4位',
          trigger: 'blur'
        }],
        Twopassword: [{
          validator: validatePass,
          trigger: 'blur'
        }],
      },
      Twopassword: '',
    }
  },
  methods: {
    async sendSms() {
      console.log("发送短信信息");
      if (this.userInfo.userTelephone == "" || this.userInfo.userTelephone == null || this.userInfo.userTelephone == undefined) {
        this.$message.error('请输入你的电话号码!');
        return;
      }
      const {
        data: res
      } = await this.$http.post(`/third/send/${this.userInfo.userTelephone}`);
      console.log("发送验证码");
      console.log(res);
      if (res.code == 0) {
        this.$message.success("发送成功!");
      } else {
        this.$message.error("发送失败!验证码未过期,请查询");
      }
    },
    async register() {
      this.$refs['userInfoRef'].validate(async (valid) => {
        if (valid) {
          // 去查询这个验证码是否正确，如果不正确 返回错误
          const {
            data: res1
          } = await this.$http.get(`third/phoneCode/${this.userInfo.userTelephone}`);
          if (res1.smscode != this.userInfo.code) {
            this.$message.error('验证码错误！请重新输入');
            this.code = "";
            return;
          }
          const {
            data: res
          } = await this.$http.post('person/generator/user/save', this.userInfo);
          console.log(res);
          if (res.code == 0) {
            console.log('注册成功!');
            const loading = this.$loading({
              lock: true,
              text: '注册成功',
              spinner: 'el-icon-loading',
              background: 'rgba(0, 0, 0, 0.7)'
            });
            setTimeout(() => {
              loading.close();
              window.location.href = './login.html';
            }, 2000);
            // this.$message.success('注册成功!');
          } else {
            console.log('注册失败!');
            this.$message.error('注册失败,' + res.msg);
            this.userInfo.username = "";
            this.userInfo.password = "";
            this.userInfo.userTelephone = "";
            this.userInfo.email = "";
          }
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },

  },
}
var vm = Vue.extend(main);
new vm().$mount('#app');