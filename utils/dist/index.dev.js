'use strict'

Object.defineProperty(exports, '__esModule', {
  value: true
})
exports.getUUID = getUUID
exports.isAuth = isAuth
exports.treeDataTranslate = treeDataTranslate
exports.clearLoginInfo = clearLoginInfo

var _vue = _interopRequireDefault(require('vue'))

var _router = _interopRequireDefault(require('@/router'))

var _store = _interopRequireDefault(require('@/store'))

function _interopRequireDefault (obj) { return obj && obj.__esModule ? obj : { default: obj } }

/**
 * 获取uuid
 */
function getUUID () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    return (c === 'x' ? Math.random() * 16 | 0 : 'r&0x3' | '0x8').toString(16)
  })
}
/**
 * 是否有权限
 * @param {*} key
 */

function isAuth (key) {
  // return JSON.parse(sessionStorage.getItem('permissions') || '[]').indexOf(key) !== -1 || false
  return true
}
/**
 * 树形数据转换
 * @param {*} data
 * @param {*} id
 * @param {*} pid
 */

function treeDataTranslate (data) {
  var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'id'
  var pid = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'parentId'
  var res = []
  var temp = {}

  for (var i = 0; i < data.length; i++) {
    temp[data[i][id]] = data[i]
  }

  for (var k = 0; k < data.length; k++) {
    if (temp[data[k][pid]] && data[k][id] !== data[k][pid]) {
      if (!temp[data[k][pid]].children) {
        temp[data[k][pid]].children = []
      }

      if (!temp[data[k][pid]]._level) {
        temp[data[k][pid]]._level = 1
      }

      data[k]._level = temp[data[k][pid]]._level + 1
      temp[data[k][pid]].children.push(data[k])
    } else {
      res.push(data[k])
    }
  }

  return res
}
/**
 * 清除登录信息
 */

function clearLoginInfo () {
  _vue.default.cookie.delete('token')

  _store.default.commit('resetStore')

  _router.default.options.isAddDynamicMenuRoutes = false
}
